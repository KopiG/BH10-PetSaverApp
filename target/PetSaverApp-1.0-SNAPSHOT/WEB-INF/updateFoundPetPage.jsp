<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import = "java.io.*,java.util.*" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>UpdateFoundPetPage</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Amatic+SC">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <style>
            body, html {height: 100%}
            body,h1,h2,h3,h4,h5,h6 {font-family: "Amatic SC", sans-serif}
            
            .updateFoundPetPage{
            background-color: #F5F5DC;
        }
        
        </style>
 
    </head>
    <body class="updateFoundPetPage">
        <!-- Navbar (sit on top) -->
        <div class="w3-top w3-hide-small">
            <div class="w3-bar w3-xlarge w3-black w3-hover-opacity-off" id="myNavbar">
                <a href="homePageServlet" class="nav-link w3-bar-item w3-button w3-padding-large">Főoldal<span class="sr-only"></span></a>
                <a href="lostPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Elveszett</a>
                <a href="foundPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Megtalált</a>
                <a href="storyPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Sikertörténetek</a>
                <a href="aboutUsPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Rólunk</a>
                <c:if test="${pageContext.request.isUserInRole('user') || pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarUserPage.jsp" />
                    </div>
                </c:if>
                
                <c:if test="${pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarAdminPage.jsp" />
                    </div>
                </c:if>
                
                <div id="topmenu_navbar">
                    <jsp:include page="navBarLoginRegistrationPage.jsp" />
                </div>
            </div>
        </div>
        

        <div class="container-fluid text-center">    
            <div class="row content">
                <div class="col-sm-3"></div>
                <div class="w3-container rapid w3-padding-20 w3-card">
                    <p style="font-size:2vw; color:white" >Megtalált</p>
                    <h1>Megtalált kisállat adatlap szerkesztése:</h1>
                    <h2>Gyors bejelentőtől kapott adatok:</h2>
                    <div>
                        <table id="announced">
                            <th></th>
                            <th>Kisállat neme</th>
                            <th>Megtalált hely</th>
                            <th>Megtalált időpont</th>
                            <th>Kisállat tulajdonságai</th>                      
                            <th>Megtaláló</th>
                            <tr>
                                <td><image src="DisplayImageServlet?name=${pet.image}" data-toggle="modal" data-target="#myModal" style="height:100px; width:100px;"></td>
                                <td>${pet.sex}</td>
                                <td>${pet.foundPlace}</td>                          
                                <td>${pet.foundDate}</td>
                                <td>${pet.petProperties}</td>
                                <td>${pet.announcer.getEmailOfAnnouncer()}</td>
                            </tr>
                        </table>
                    </div>
                        <div>
                            <a href="userPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Vissza a listákhoz</a>
                        </div>        
                            </br>
                                                        
                    <div id="foundForm">
                        
                        <form  action="foundPageServlet" method="post">
                            <label for="nickName">Kisállat neve:
                                <input id="petName" name="nickName" class="textfield" type="text">
                            </label>
                            <br/>
                            <label for="catOrDog">Kutya vagy macska?
                            <select name="catOrDog">
                                <option value="TRUE">kutya</option>
                                <option value="FALSE">macska</option>                              
                            </select>
                            </label>
                            <br/>
                            <label for="age">Kor:
                                <input id="age" name="age" class="textfield" type="text">
                            </label>
                            <br/>                          
                            <label for="color">Szín:
                                <input id="color" name="color" class="textfield" type="text">
                            </label>
                            <br/>
                            <div class="form-group">
                                <label for="sex">Állat neme:</label>
                                <label class="container">kan
                                    <input type="radio" value="TRUE" name="sex" id="sex" ${pet.sex=='true'?'checked':''}>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container">szuka
                                    <input type="radio"  value="FALSE" name="sex" id="sex" ${pet.sex=='false'?'checked':''}>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <br/>
                            <label for="weight">Súly:
                                <input id="weight" name="weight" class="textfield" type="text">
                            </label>
                            <br/>
                            <label for="type">type: 
                                <input id="subtype" name="type" class="textfield" type="text">
                            </label>
                            <br/>
                            <label for="subType">subtype: 
                                <input id="subType" name="subType" class="textfield" type="text">
                            </label>
                            <br/>
                            <label for="dangerous">Veszélyes?
                            <select name="dangerous">
                                <option value="FALSE">nem</option>
                                <option value="TRUE">igen</option>
                                
                            </select>
                            </label>
                            <br/>
                            <label for="image">Képlink:
                                <input id="image" name="image" class="textfield" type="text" value="${pet.image}">
                            </label>
                            <br/>
                            <label for="foundPlace">Megtalálás helye:
                                <input id="foundPlace" name="foundPlace" class="textfield" type="text" value="${pet.foundPlace}">
                            </label>
                            <br/>
                            <label for="foundDate">Ekkor találták meg:
                                <input id="foundDate" name="foundDate" type="date" value="${pet.foundDate}">
                            </label>
                            <br/>
                            <label for="foundNote">Megtalálás körülményei:
                                <input id="foundNote" name="foundNote" class="textfield" type="text">
                            </label>
                            <br/>                           
                            <label for="owned">Volt már örökbefogadva: 
                                <input id="owned" name="owned" class="textfield" type="text">
                            </label>
                            <br/>
                            <label for="chipNumber">Chip szám:
                                <input id="chipNumber" name="chipNumber" class="textfield" type="text">
                            </label>
                            <br/>
                            <label for="chipDate">Chipelés dátuma:
                                <input id="chipDate" name="chipDate" class="textfield" type="date">
                            </label>
                            <br/>
                            <label for="healthDiagnoseDate">Egészségi felmérés dátuma:
                                <input id="healthDiagnoseDate" name="healthDiagnoseDate" class="textfield" type="date">
                            </label>
                            <br/>
                            <label for="healthDiagnoseCause">Egészségi felmérés oka:
                                <input id="healthDiagnoseCause" name="healthDiagnoseCause" class="textfield" type="text">
                            </label>
                            <br/>  
                            <label for="healthDiagnoseResult">Egészségi felmérés eredménye:
                                <input id="healthDiagnoseResult" name="healthDiagnoseResult" class="textfield" type="text">
                            </label>
                            <br/>
                            <label for="vaccinationDate">Oltás dátuma:
                                <input id="vaccinationDate" name="vaccinationDate" class="textfield" type="date">
                            </label>
                            <br/>
                            <label for="vaccinationType">Oltás tipusa:
                                <input id="vaccinationType" name="vaccinationType" class="textfield" type="text">
                            </label>
                            <br/>
                            <input type="hidden" id="fosterid" name="fosterid" value="">
                            <br/>
                            <label for="userEmail">Felelős önkéntes:
                            <select name="userEmail">
                                <option value="" selected disabled hidden>Felelős önkéntes:</option>
                                <option value="orsi@gmail.com" selected>orsi@gmail.com</option>
                                <option value="jani@gmail.com">jani@gmail.com</option>
                                <option value="zoli@gmail.com">zoli@gmail.com</option>
                                <option value="user@gmail.com">user@gmail.com</option>
                            </select>
                            </label>
                            <br/>
                            <input type="hidden" id="announcerid" name="announcerid" value="${Long.valueOf(pet.announcer.getAnnouncerId())}">
                            <br/>
                            <input type="hidden" id="petId" name="petId" value="${foundId}">                           
                            <br/>
                            <button type="submit">Adatlap mentése</button>
                            <input type="button" onclick="location.href='foundPageServlet';" value="Vissza" />
      
                        </form>
                    </div>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </div>
    </body>
</html>
