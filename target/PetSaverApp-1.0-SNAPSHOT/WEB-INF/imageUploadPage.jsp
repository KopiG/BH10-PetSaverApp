<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import = "java.io.*,java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>FileUpload</title>
        <meta http-equiv="Content-Type" content="text/html" >
        <script type="text/javascript">
            function alertUpload() {
                alert("Sikeres feltöltés!");
            }
        </script>
    </head>
    <body>
        <div id="myUploadDialog" title="FileUpload">
            <form action="fileUploadHandlerServlet" enctype="multipart/form-data" method="post" onsubmit="alertUpload()">
                <input type="file" name="file2" /><br>
                <input type="submit" value="upload" />
            </form>
        </div>
    </body>
</html>