<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import = "java.io.*,java.util.*" %>
<!DOCTYPE html>
<html>
    <head>
        <title>RegistrationPage</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Amatic+SC">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/registration.css"/>
        <style>
            .registration-w3-top{
                font-family: "Amatic SC", sans-serif;
            }

            .regisztracio{
                background-image: url("resources/media/funnyCats.jpg");
                font-family: Arial;
                min-height: 100vh;
                background-size: cover;
                padding-top: 8%;
                padding-button: 8%;
            }

            .registration-card{
                background-color: rgba(42, 47, 51, .8);
                color: #fff;
            }

            .registration-card a{
                color: #eff0f7;
            }

            .registration-card-header{
                background-color: rgba(42, 47, 51, .8);
                letter-spacing: 3px;
                font-size: 1.2rem;
                font-weight: 600;
                color: #eff0f7;
            }

            .registration-card .form-control{
                background-color:rgba(42, 47, 51, .7);
                border-color: transparent;
            }

            .registration-card .form-control:focus{
                background-color:rgba(42, 47, 51, .5);
                border-color: #3384AA;
                outline: none;
                box-shadow: none;
                color: #eff0f7;
            }

            .registration-card .form-control::placeholder{
                color: #d6d7de;
                opacity: .8;
            }

            .btn-registration{
                background-color: #298e62;
                letter-spacing: 3px;
                font-weight: 600;
            }

            .btn-registration:hover{
                background-color: #2d9b6b;
            }
        </style>
    </head>
    <body class="regisztracio">
        <div class="container">    
            <div class="row">
                <div class="col-12 col-lg-6 mx-auto">
                    <div class="card registration-card">
                        <div class="card-header registration-card-header text-uppercase">
                            Regisztráció
                        </div>
                        <form  name="fosterRegistration" action="fosterRegistrationServlet" method="post">
                            <div class="card-body">
                                <section class="color-1">
                                    <div class="form-group">
                                        <label for="firstName">Keresztnév: </label>
                                        <input class="form-control" id="firstName" name="firstName" class="textfield" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="lastName">Vezetéknév: </label>
                                        <input class="form-control" id="lastName" name="lastName" class="textfield" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">Telefonszám: </label>
                                        <input class="form-control" id="phone" name="phone" class="textfield" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">E-mail cím:</label>
                                        <input class="form-control" id="email" name="email" class="textfield" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="note">megjegyzés:</label>
                                        <input class="form-control" id="note" name="note" class="textfield" type="text">
                                    </div>
                                    <div class="card-footer">
                                        <input type="submit" name="submit" value="Regisztráció" type="button" class="btn btn-registration btn-secondary btn-lg btn-block text-uppercase">
                                    </div>
                                </section>    
                            </div>        
                        </form>>    
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
