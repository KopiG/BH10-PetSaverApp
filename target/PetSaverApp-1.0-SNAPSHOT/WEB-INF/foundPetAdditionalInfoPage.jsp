<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import = "java.io.*,java.util.*" %>
<!DOCTYPE html>
<html>
    <head>
        <title>FoundPetAdditionalInfo</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Amatic+SC">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <style>
            body, html {height: 100%}
            body,h1,h2,h3,h4,h5,h6 {font-family: "Amatic SC", sans-serif}

            * {
                box-sizing: border-box;
            }

            body {
                font-family: Arial, Helvetica, sans-serif;
            }

            /* Style the header */
            .header {
                background-color: #f1f1f1;
                padding: 30px;
                text-align: center;
                font-size: 35px;
            }

            /* Container for flexboxes */
            .row {
                display: -webkit-flex;
                display: flex;
            }

            /* Create three equal columns that sits next to each other */
            .column {
                -webkit-flex: 1;
                -ms-flex: 1;
                flex: 1;
                padding: 10px;
                height: 300px; /* Should be removed. Only for demonstration */
            }

            /* Style the footer */
            .footer {
                background-color: #f1f1f1;
                padding: 10px;
                text-align: center;
            }

            /* Responsive layout - makes the three columns stack on top of each other instead of next to each other */
            @media (max-width: 600px) {
                .row {
                    -webkit-flex-direction: column;
                    flex-direction: column;
                }
            }

        </style>
    </head>
    <body>
        <!-- Navbar (sit on top) -->
        <div class="w3-top w3-hide-small">
            <div class="w3-bar w3-xlarge w3-black w3-opacity w3-hover-opacity-off" id="myNavbar">
                <a href="homePageServlet" class="nav-link w3-bar-item w3-button w3-padding-large">Főoldal<span class="sr-only"></span></a>
                <a href="lostPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Elveszett</a>
                <a href="foundPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Megtalált</a>
                <a href="storyPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Sikertörténetek</a>
                <a href="aboutUsPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Rólunk</a>
                <c:if test="${pageContext.request.isUserInRole('user') || pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarUserPage.jsp" />
                    </div>
                </c:if>

                <c:if test="${pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarAdminPage.jsp" />
                    </div>
                </c:if>

                <div id="topmenu_navbar">
                    <jsp:include page="navBarLoginRegistrationPage.jsp" />
                </div>
            </div>
        </div>

        <div class="header">
            <h2>Header</h2>
        </div>
        <c:forEach items="${foundPetsList}" var="item">
            <c:url var="updateLink" value="foundPageServlet">
                <c:param name="catOrDog" value="${item.catOrDog}"/>
                <c:param name="type" value="${item.type}"/>
                <c:param name="sex" value="${item.sex}"/>
                <c:param name="age" value="${item.age}"/>
                <c:param name="color" value="${item.color}"/>
                <c:param name="weight" value="${item.weight}"/>
                <c:param name="image" value="${item.image}"/>
                <c:param name="dangerous" value="${item.dangerous}"/>

            </c:url>
            <div class="row">
                <div class="column" style="background-color:#bbb;">
                    <div>
                        <p>Kisallat foto</p>
                        <td><image src="DisplayImageServlet?name=${item.image}" data-toggle="modal" data-target="#myModal" style="height:250px; width:250px;"></td>       

                    </div>
                </div>
                <div class="column" style="background-color:#ccc;">
                    <div>
                        <p>Kisallat adatok</p>
                        <table class="table">
                            <tr>
                                <td>Tipus</td>
                                <td>${item.type}</td>
                            </tr>	
                            <tr>	
                                <td>Neme</td>
                                <c:if test="${item.sex == 'TRUE'}">
                                    <td>Him</td>
                                </c:if>
                                <c:if test="${item.sex == 'FALSE'}">
                                    <td>Nőstény</td>
                                </c:if>
                            </tr>	
                            <tr>	
                                <td>Kora</td>
                                <td>${item.age}</td>
                            </tr>	
                            <tr>	
                                <td>Szine</td>
                                <td>${item.color}</td>
                            </tr>	
                            <tr>	
                                <td>Súlya</td>							
                                <td>${item.weight}</td>
                            </tr>
                            <tr>
                                <c:if test="${item.catOrDog == 'TRUE'}">
                                    <td>Kutya</td>
                                </c:if>
                                <c:if test="${item.catOrDog == 'FALSE'}">
                                    <td>Macska</td>
                                </c:if> 
                            </tr>
                            <tr>	
                                <td>Veszélyes</td>							
                                <td>${item.dangerous}</td>
                            </tr>


                        </table>
                    </div>
                </div>
            </div>
        </c:forEach>
    </table>
    <div>
        <button type="button" value="Submit">Érdekel</button>
    </div>

    <!-- Footer -->
    <footer class="w3-container w3-padding-64 w3-buttom w3-opacity w3-light-grey w3-xlarge">
        <div class="footer-copyright text-center py-3">© 2019 Copyright:
            <a href="https://wwf.hu/" style="color:#009900"> PetSaver</a>
        </div>
    </footer>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <img class="showimage img-responsive" src="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function () {
            $('img').on('click', function () {
                var image = $(this).attr('src');
                //alert(image);
                $('#myModal').on('show.bs.modal', function () {
                    $(".showimage").attr("src", image);
                });
            });
        });
    </script>

</body>
</html>
