package hu.bh10.petsaverapp.entity;

import static hu.bh10.petsaverapp.entity.RescueCircumstanceEntity.NQ_GET_RESCUES_BY_ID;
import static hu.bh10.petsaverapp.entity.RescueCircumstanceEntity.NQ_GET_RESCUE_BY_ID;
import static hu.bh10.petsaverapp.entity.RescueCircumstanceEntity.PARAM_PET_ID;
import static hu.bh10.petsaverapp.entity.RescueCircumstanceEntity.PARAM_RESCUE_ID;
import java.sql.Date;
import java.time.LocalTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "rescue_circumstance")
@NamedQueries({
    @NamedQuery(name = NQ_GET_RESCUES_BY_ID, query = "select r from RescueCircumstanceEntity r where r.rescuedPet.id =:"+ PARAM_PET_ID),
    @NamedQuery(name = NQ_GET_RESCUE_BY_ID, query = "select r from RescueCircumstanceEntity r where r.id =:"+ PARAM_RESCUE_ID)
})
public class RescueCircumstanceEntity extends BaseEntity{
    
    public static final String NQ_GET_RESCUES_BY_ID = "RescueCircumstanceEntity.getRescues";
    public static final String PARAM_PET_ID = "petId";
    public static final String NQ_GET_RESCUE_BY_ID = "RescueCircumstanceEntity.getRescue";
    public static final String PARAM_RESCUE_ID = "rescueId";
    
    @Column(name = "rescue_date", nullable = false)
    private Date rescueDate;
    
    @Column(name = "rescue_time")
    private LocalTime rescueTime;
    
    @Column(name = "rescue_place", nullable = false)
    private String rescuePlace;
    
    @Column(name = "rescue_note")
    private String rescueNote;
    
    @JoinColumn(name = "pet_id")
    @OneToOne(fetch = FetchType.EAGER)
    private PetEntity rescuedPet;

    public Date getRescueDate() {
        return rescueDate;
    }

    public void setRescueDate(Date rescueDate) {
        this.rescueDate = rescueDate;
    }

    public LocalTime getRescueTime() {
        return rescueTime;
    }

    public void setRescueTime(LocalTime rescueTime) {
        this.rescueTime = rescueTime;
    }

    public String getRescuePlace() {
        return rescuePlace;
    }

    public void setRescuePlace(String rescuePlace) {
        this.rescuePlace = rescuePlace;
    }

    public String getRescueNote() {
        return rescueNote;
    }

    public void setRescueNote(String rescueNote) {
        this.rescueNote = rescueNote;
    }

    public PetEntity getRescuedPet() {
        return rescuedPet;
    }

    public void setRescuedPet(PetEntity rescuedPet) {
        this.rescuedPet = rescuedPet;
    }

    

  
    
    
}
