package hu.bh10.petsaverapp.entity;

import static hu.bh10.petsaverapp.entity.FosterEntity.NQ_CHECK_FOSTER_BY_EMAIL;
import static hu.bh10.petsaverapp.entity.FosterEntity.NQ_GET_FOSTER_BY_EMAIL;
import static hu.bh10.petsaverapp.entity.FosterEntity.PARAM_FOSTER_EMAIL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "foster")
@NamedQueries({
    @NamedQuery(name = NQ_GET_FOSTER_BY_EMAIL, query = "select f from FosterEntity f where f.fosterEmail = :"+ PARAM_FOSTER_EMAIL),
    @NamedQuery(name = NQ_CHECK_FOSTER_BY_EMAIL, query = "select count(f) from FosterEntity f where f.fosterEmail = :"+ PARAM_FOSTER_EMAIL)
})
public class FosterEntity extends BaseEntity{
    
    public static final String NQ_GET_FOSTER_BY_EMAIL = "FosterEntity.getFosterByEmail";
    public static final String NQ_CHECK_FOSTER_BY_EMAIL = "FosterEntity.checkFosterByEmail";
    public static final String PARAM_FOSTER_EMAIL = "fosterEmail";
    
    @OneToMany(mappedBy = "handlerFoster")
    private List<PetEntity>pets = new ArrayList<PetEntity>();
    
    @Column(name = "foster_first_name", nullable = false)
    private String fosterFirstName;
    
    @Column(name = "foster_last_name", nullable = false)
    private String fosterLastName;
    
    @Column(name = "foster_date")
    private Date fosterDate;
    
    @Column(name = "foster_note")
    private String fosterNote;
    
    @Column(name = "foster_email", nullable = false)
    private String fosterEmail;
    
    @Column(name = "foster_phone")
    private String fosterPhone;

    public String getFosterFirstName() {
        return fosterFirstName;
    }

    public void setFosterFirstName(String fosterFirstName) {
        this.fosterFirstName = fosterFirstName;
    }

    public String getFosterLastName() {
        return fosterLastName;
    }

    public void setFosterLastName(String fosterLastName) {
        this.fosterLastName = fosterLastName;
    }

    public Date getFosterDate() {
        return fosterDate;
    }

    public void setFosterDate(Date fosterDate) {
        this.fosterDate = fosterDate;
    }

    public String getFosterNote() {
        return fosterNote;
    }

    public void setFosterNote(String fosterNote) {
        this.fosterNote = fosterNote;
    }

    public String getFosterEmail() {
        return fosterEmail;
    }

    public void setFosterEmail(String fosterEmail) {
        this.fosterEmail = fosterEmail;
    }

    public String getFosterPhone() {
        return fosterPhone;
    }

    public void setFosterPhone(String fosterPhone) {
        this.fosterPhone = fosterPhone;
    }
    
    
}
