package hu.bh10.petsaverapp.entity;

import static hu.bh10.petsaverapp.entity.PetEntity.NQ_GET_FOUND_OR_LOST_PETS;
import static hu.bh10.petsaverapp.entity.PetEntity.NQ_GET_LAST_SAVED_PET;
import static hu.bh10.petsaverapp.entity.PetEntity.NQ_GET_MAX_PET_ID;
import static hu.bh10.petsaverapp.entity.PetEntity.NQ_GET_PET_BY_ID;
import static hu.bh10.petsaverapp.entity.PetEntity.PARAM_PET_FOUND_LOST_TYPE;
import static hu.bh10.petsaverapp.entity.PetEntity.PARAM_PET_ID;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "pet")
@NamedQueries({
    @NamedQuery(name = NQ_GET_LAST_SAVED_PET, query = "select p from PetEntity p where p.id = max(p.id)"),
    @NamedQuery(name = NQ_GET_PET_BY_ID, query = "select p from PetEntity p where p.id = :"+ PARAM_PET_ID),
    @NamedQuery(name = NQ_GET_MAX_PET_ID, query = "select max(p.id) from PetEntity p"),
    @NamedQuery(name = NQ_GET_FOUND_OR_LOST_PETS, query = "select p from PetEntity p where p.found =: "+PARAM_PET_FOUND_LOST_TYPE)
})
public class PetEntity extends BaseEntity{
    
    public static final String NQ_GET_LAST_SAVED_PET = "PetEntity.getLastSavedPet";
    public static final String NQ_GET_PET_BY_ID = "PetEntity.getPetById";
    public static final String NQ_GET_MAX_PET_ID = "PetEntity.getMaxPetId";
    public static final String NQ_GET_FOUND_OR_LOST_PETS = "PetEntity.getFoundOrLostPets";
    public static final String PARAM_PET_ID = "petId";
    public static final String PARAM_PET_FOUND_LOST_TYPE = "petFoundLostType";
    
    @Column(name = "nick_name", nullable = false)
    private String nickName;
    
    @Column(name = "cat_or_dog", nullable = false)
    private String catOrDog;
    
    @Column(nullable = false)
    private int age;
    
    @Column(nullable = false)
    private String color;
    
    @Column(nullable = false)
    private String sex;
    
    @Column
    private double weight;
    
    @Column
    private String type;
    
    @Column(name = "sub_type")
    private String subtype;
    
    @Column
    private String owned;
    
    @Column
    private String dangerous;

    @Column(nullable = false)
    private String image;
    
    @Column(name = "interest_counter")
    private int interestCounter;
    
    @Column(name = "success")
    private String success;
    
    @Column(name = "found")
    private String found;
    
    @JoinColumn(name = "user_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private UserEntity handlerUser;
    
    @JoinColumn(name = "announcer_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private AnnouncerEntity handlerAnnouncer;
    
    @JoinColumn(name = "foster_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private FosterEntity handlerFoster;
    
    
    
    @OneToMany(mappedBy = "rescuedPet", cascade = CascadeType.ALL)
    private List<RescueCircumstanceEntity>circumstances = new ArrayList<RescueCircumstanceEntity>();
    
    @OneToMany(mappedBy = "chipedPet")
    private List<ChipEntity>chips = new ArrayList<ChipEntity>();
    
    @OneToMany(mappedBy = "diagnosedPet")
    private List<HealthDiagnoseEntity>diagnoses = new ArrayList<HealthDiagnoseEntity>();
    
    @OneToMany(mappedBy = "vaccinatedPet")
    private List<VaccinationEntity>vaccinations = new ArrayList<VaccinationEntity>();


    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getCatOrDog() {
        return catOrDog;
    }

    public void setCatOrDog(String catOrDog) {
        this.catOrDog = catOrDog;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getOwned() {
        return owned;
    }

    public void setOwned(String owned) {
        this.owned = owned;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getFound() {
        return found;
    }

    public void setFound(String found) {
        this.found = found;
    }
    
    

    public FosterEntity getHandlerFoster() {
        return handlerFoster;
    }

    public void setHandlerFoster(FosterEntity handlerFoster) {
        this.handlerFoster = handlerFoster;
    }

    public List<RescueCircumstanceEntity> getCircumstances() {
        return circumstances;
    }

    public void setCircumstances(List<RescueCircumstanceEntity> circumstances) {
        this.circumstances = circumstances;
    }

    public List<ChipEntity> getChips() {
        return chips;
    }

    public void setChips(List<ChipEntity> chips) {
        this.chips = chips;
    }

    public List<HealthDiagnoseEntity> getDiagnoses() {
        return diagnoses;
    }

    public void setDiagnoses(List<HealthDiagnoseEntity> diagnoses) {
        this.diagnoses = diagnoses;
    }

    public List<VaccinationEntity> getVaccinations() {
        return vaccinations;
    }

    public void setVaccinations(List<VaccinationEntity> vaccinations) {
        this.vaccinations = vaccinations;
    }

    public AnnouncerEntity getHandlerAnnouncer() {
        return handlerAnnouncer;
    }

    public void setHandlerAnnouncer(AnnouncerEntity handlerAnnouncer) {
        this.handlerAnnouncer = handlerAnnouncer;
    }
    
    public String getDangerous() {
        return dangerous;
    }

    public void setDangerous(String dangerous) {
        this.dangerous = dangerous;
    }

    public int getInterestCounter() {
        return interestCounter;
    }

    public void setInterestCounter(int interestCounter) {
        this.interestCounter = interestCounter;
    }

    public UserEntity getHandlerUser() {
        return handlerUser;
    }

    public void setHandlerUser(UserEntity handlerUser) {
        this.handlerUser = handlerUser;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
    
}
