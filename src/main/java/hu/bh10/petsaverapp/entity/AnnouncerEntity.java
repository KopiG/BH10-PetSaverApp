package hu.bh10.petsaverapp.entity;

import static hu.bh10.petsaverapp.entity.AnnouncerEntity.NQ_CHECK_ANNOUNCER_EMAIL;
import static hu.bh10.petsaverapp.entity.AnnouncerEntity.NQ_GET_ANNOUNCER_BY_ID;
import static hu.bh10.petsaverapp.entity.AnnouncerEntity.NQ_GET_ANNOUNCER_BY_MAIL;
import static hu.bh10.petsaverapp.entity.AnnouncerEntity.NQ_GET_ANNOUNCER_ID_BY_EMAIL;
import static hu.bh10.petsaverapp.entity.AnnouncerEntity.PARAM_ANNOUNCER_EMAIL;
import static hu.bh10.petsaverapp.entity.AnnouncerEntity.PARAM_ANNOUNCER_ID;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "announcer")
@NamedQueries({
    @NamedQuery(name = NQ_GET_ANNOUNCER_BY_ID, query = "select a from AnnouncerEntity a where a.id =:"+ PARAM_ANNOUNCER_ID),
    @NamedQuery(name = NQ_GET_ANNOUNCER_BY_MAIL, query = "select a from AnnouncerEntity a where a.emailOfAnnouncer =:"+ PARAM_ANNOUNCER_EMAIL),
    @NamedQuery(name = NQ_GET_ANNOUNCER_ID_BY_EMAIL, query = "select a.id from AnnouncerEntity a where a.emailOfAnnouncer =:"+ PARAM_ANNOUNCER_EMAIL),
    @NamedQuery(name = NQ_CHECK_ANNOUNCER_EMAIL, query = "select a from AnnouncerEntity a where a.emailOfAnnouncer =:"+ PARAM_ANNOUNCER_EMAIL)
})
public class AnnouncerEntity extends BaseEntity{
    
    public static final String NQ_GET_ANNOUNCER_BY_ID = "AnnouncerEntity.getAnnouncer";
    public static final String NQ_GET_ANNOUNCER_BY_MAIL = "AnnouncerEntity.getAnnouncerByMail";
    public static final String PARAM_ANNOUNCER_ID = "announcerId";
    public static final String NQ_GET_ANNOUNCER_ID_BY_EMAIL = "AnnouncerEntity.getAnnouncerId";
    public static final String NQ_CHECK_ANNOUNCER_EMAIL = "AnnouncerEntity.checkAnnouncerEmail";
    public static final String PARAM_ANNOUNCER_EMAIL = "announcerEmail";
    
    @OneToMany(mappedBy = "lostAnnouncer", cascade = CascadeType.ALL)
    private List<LostEntity>losts = new ArrayList<LostEntity>();
    
    @OneToMany(mappedBy = "foundAnnouncer", cascade = CascadeType.ALL)
    private List<FoundEntity>founds = new ArrayList<FoundEntity>();
    
    @OneToMany(mappedBy = "handlerAnnouncer")
    private List<PetEntity>pets = new ArrayList<PetEntity>();
    
    @Column(name = "name_of_announcer", nullable = false)
    private String nameOfAnnouncer;
    
    @Column(name = "phone_of_announcer", nullable = false)
    private String phoneOfAnnouncer;
    
    @Column(name = "email_of_announcer", nullable = false)
    private String emailOfAnnouncer;

    public AnnouncerEntity() {
    }

    public AnnouncerEntity(String nameOfAnnouncer, String phoneOfAnnouncer, String emailOfAnnouncer) {
        this.nameOfAnnouncer = nameOfAnnouncer;
        this.phoneOfAnnouncer = phoneOfAnnouncer;
        this.emailOfAnnouncer = emailOfAnnouncer;
    }
    
    

    public String getNameOfAnnouncer() {
        return nameOfAnnouncer;
    }

    public void setNameOfAnnouncer(String nameOfAnnouncer) {
        this.nameOfAnnouncer = nameOfAnnouncer;
    }

    public String getPhoneOfAnnouncer() {
        return phoneOfAnnouncer;
    }

    public void setPhoneOfAnnouncer(String phoneOfAnnouncer) {
        this.phoneOfAnnouncer = phoneOfAnnouncer;
    }

    public String getEmailOfAnnouncer() {
        return emailOfAnnouncer;
    }

    public void setEmailOfAnnouncer(String emailOfAnnouncer) {
        this.emailOfAnnouncer = emailOfAnnouncer;
    }
    
    
    
}
