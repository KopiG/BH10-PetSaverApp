package hu.bh10.petsaverapp.entity;

import static hu.bh10.petsaverapp.entity.UserEntity.NQ_CHECK_USER_BY_EMAIL;
import static hu.bh10.petsaverapp.entity.UserEntity.NQ_GET_USERS;
import static hu.bh10.petsaverapp.entity.UserEntity.NQ_GET_USER_BY_EMAIL;
import static hu.bh10.petsaverapp.entity.UserEntity.PARAM_USER_EMAIL;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user")
@NamedQueries({
    @NamedQuery(name = NQ_GET_USERS, query = "select u from UserEntity u"),
    @NamedQuery(name = NQ_GET_USER_BY_EMAIL, query = "select u from UserEntity u where u.userEmail = :"+ PARAM_USER_EMAIL),
    @NamedQuery(name = NQ_CHECK_USER_BY_EMAIL, query = "select count(u) > 0 from UserEntity u where u.userEmail = :"+ PARAM_USER_EMAIL)
})
public class UserEntity {
    //select c FROM Course c JOIN c.enrolledStudents u WHERE u.id = :userId
    public static final String NQ_GET_USERS = "UserEntity.getUsers";
    public static final String NQ_GET_USER_BY_EMAIL = "UserEntity.findUserByEmail";
    public static final String NQ_CHECK_USER_BY_EMAIL = "UserEntity.checkUserByEmail";
    public static final String PARAM_USER_EMAIL = "email";
    
    @OneToMany(mappedBy = "handlerUser")
    private List<PetEntity>pets = new ArrayList<PetEntity>();

    @Column(name = "user_last_name", nullable = false)
    private String userLastName;

    @Column(name = "user_first_name", nullable = false)
    private String userFirstName;

    @Column(name = "nick_name", nullable = false)
    private String nickName;

    @Column(name = "user_password", nullable = false)
    private String userPassword;
    
    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_email", unique = true, nullable = false)
    private String userEmail;

    @Column
    private String role;
    
    @Column
    private String valid;
    
    @ManyToMany(mappedBy = "userGroups")
    private List<UserGroup> groups;
    
//    @JoinColumn(name = "user_role")
//    @ManyToOne(fetch = FetchType.EAGER)
//    private UserGroup userRole;

    public UserEntity() {
    }

    public UserEntity(String userLastName, String userFirstName, String nickName, String userPassword, String userEmail, String role, String valid) {
        this.userLastName = userLastName;
        this.userFirstName = userFirstName;
        this.nickName = nickName;
        this.userPassword = userPassword;
        this.userEmail = userEmail;
        this.role = role;
        this.valid = valid;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getEmail() {
        return userEmail;
    }

    public void setEmail(String email) {
        this.userEmail = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<PetEntity> getPets() {
        return pets;
    }

    public void setPets(List<PetEntity> pets) {
        this.pets = pets;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }

    public List<UserGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<UserGroup> groups) {
        this.groups = groups;
    }
    

    @Override
    public String toString() {
        return "UserEntity{" + "userLastName=" + userLastName + ", userFirstName=" + userFirstName + ", nickName=" + nickName + ", userPassword=" + userPassword + ", userEmail=" + userEmail + ", role=" + role + ", valid=" + valid + '}';
    }

}
