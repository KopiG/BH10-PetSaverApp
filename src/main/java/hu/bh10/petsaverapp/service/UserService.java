package hu.bh10.petsaverapp.service;

import hu.bh10.petsaverapp.dao.UserDAO;
import hu.bh10.petsaverapp.dto.UserDTO;
import hu.bh10.petsaverapp.entity.UserEntity;
import hu.bh10.petsaverapp.mapper.UserMapper;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Singleton
public class UserService {
    
    @Inject
    UserDAO userDAO;

    public UserService() {
    }

    public UserService(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
    
    
    
     public UserEntity registerUser(UserDTO user){
        String nickName = user.getNickName();
        String role = user.getRole();
        String userEmail = user.getUserEmail();
        String userFirstName = user.getUserFirstName();
        String userLastName = user.getUserLastName();
        String userPassword = "";
        try {
            userPassword = encodeSHA256(user.getUserPassword());
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
        boolean valid = user.isValid();
        
        UserEntity entity = UserMapper.toEntity(nickName, role, userEmail, userFirstName, userLastName, userPassword, valid);
        
        try {
            userDAO.createEntity(entity);//throws new exception --> service osztálbyan kezelve
        } catch (Exception ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return entity;
    }
    
    public boolean authenticateUser(String email, String password) throws Exception{
        String hashPassword = encodeSHA256(password);
        System.out.println("Ezt a függvényt használjuk?");//NEM!!!
            return userDAO.authenticate(email, hashPassword);
    }
     
    public boolean checkUserIfExist(String email) throws Exception{
        return userDAO.isExist(email);
    }
    
    private String encodeSHA256(String password) throws UnsupportedEncodingException, NoSuchAlgorithmException {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes(StandardCharsets.UTF_8));
            byte[] digest = md.digest();
            
            StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < digest.length; i++) {
                String hex = Integer.toHexString(0xff & digest[i]);
                    if(hex.length() == 1){
                        hexString.append('0');
                    }
                hexString.append(hex);
            }
            return hexString.toString();
    }
    
    public UserDTO getUserByEmail(String userEmail){
        return UserMapper.toUserDTO(userDAO.getUserByEmail(userEmail));
    }
    
}
