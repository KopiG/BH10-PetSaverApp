package hu.bh10.petsaverapp.service;

import hu.bh10.petsaverapp.dao.AnnouncerDAO;
import hu.bh10.petsaverapp.dao.LostDAO;
import hu.bh10.petsaverapp.dto.LostDTO;
import hu.bh10.petsaverapp.entity.AnnouncerEntity;
import hu.bh10.petsaverapp.entity.LostEntity;
import hu.bh10.petsaverapp.mapper.LostMapper;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class LostFormService {

    @Inject
    LostDAO lostDAO;

    @Inject
    AnnouncerDAO announcerDAO;

    public LostFormService() {
    }

    public LostFormService(LostDAO lostDAO, AnnouncerDAO announcerDAO) {
        this.lostDAO = lostDAO;
        this.announcerDAO = announcerDAO;
    }
      

    public void createLost(LostDTO lost, Long announcerId) {
        AnnouncerEntity announcerEntity = announcerDAO.findAnnouncerById(announcerId);
        LostEntity entity = LostMapper.toLostEntity(lost, announcerEntity);
        lostDAO.createEntity(entity);
    }

    public List<LostEntity> findAll() {
        return lostDAO.findAll(LostEntity.class);
    }
    
    public List<LostEntity> findAllNotAdded() {
        return lostDAO.findAllNotAdded();
    }

    public void removeLost(LostEntity lostEntity, Long id) {
        lostDAO.removeLost(lostEntity, id);
    }
    
    public LostDTO getLostById(Long id){
        return LostMapper.toLostDTO(lostDAO.getLostById(id));
    }

    public void updateLost(Long lostId){
        LostEntity entity = lostDAO.getLostById(lostId);
        entity.setAdded("true");
        lostDAO.updateLost(entity);
    }

}
