package hu.bh10.petsaverapp.service;

import hu.bh10.petsaverapp.dao.AnnouncerDAO;
import hu.bh10.petsaverapp.dao.FoundDAO;
import hu.bh10.petsaverapp.dto.FoundDTO;
import hu.bh10.petsaverapp.entity.AnnouncerEntity;
import hu.bh10.petsaverapp.entity.FoundEntity;
import hu.bh10.petsaverapp.mapper.FoundMapper;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class FoundFormService {

    @Inject
    FoundDAO foundDAO;
    
    @Inject
    AnnouncerDAO announcerDAO;

    public FoundFormService() {
    }

    public FoundFormService(FoundDAO foundDAO, AnnouncerDAO announcerDAO) {
        this.foundDAO = foundDAO;
        this.announcerDAO = announcerDAO;
    }
    
    
    public void createFound(FoundDTO found, Long announcerId) {
        AnnouncerEntity announcerEntity = announcerDAO.findAnnouncerById(announcerId);
        FoundEntity entity = FoundMapper.toFoundEntity(found, announcerEntity);       
        foundDAO.createEntity(entity);
    }

    public List<FoundEntity> findAll() {
        return foundDAO.findAll(FoundEntity.class);
    }
    
    public List<FoundEntity> findAllNotAdded() {
        return foundDAO.findAllNotAdded();
    }

    public void removeFound(FoundEntity foundEntity, Long id) {
        foundDAO.removeFound(foundEntity, id);
    }
    
    public FoundDTO getFoundById(Long id){
        return FoundMapper.toFoundDTO(foundDAO.getFoundById(id)); 
    }
    
    public void updateFound(Long foundId){
        FoundEntity entity = foundDAO.getFoundById(foundId);
        entity.setAdded("true");
        foundDAO.updateFound(entity);
    }

}
