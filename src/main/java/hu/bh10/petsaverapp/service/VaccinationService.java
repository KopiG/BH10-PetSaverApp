package hu.bh10.petsaverapp.service;

import hu.bh10.petsaverapp.dao.PetDAO;
import hu.bh10.petsaverapp.dao.VaccinationDAO;
import hu.bh10.petsaverapp.dto.VaccinationDTO;
import hu.bh10.petsaverapp.entity.PetEntity;
import hu.bh10.petsaverapp.entity.VaccinationEntity;
import hu.bh10.petsaverapp.mapper.VaccinationMapper;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class VaccinationService {
    
    @Inject
    VaccinationDAO vaccinationDAO;
    
    @Inject
    PetDAO petDAO;
    
    public void createVaccination(VaccinationDTO vaccination, Long petId) {
        PetEntity petEntity = petDAO.getPetById(petId);
        VaccinationEntity entity = VaccinationMapper.toEntity(vaccination, petEntity);
        vaccinationDAO.createEntity(entity);
    }
    
    public void createVaccination(String vaccinationDateString, String vaccinationType, Long petId) {
        if((vaccinationDateString!=null && !vaccinationDateString.isEmpty()) && (vaccinationType!=null && !vaccinationType.isEmpty())){
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate vaccinationDate = LocalDate.parse(vaccinationDateString, dtf);
            VaccinationDTO vaccinationDTO = new VaccinationDTO();
            vaccinationDTO.setVaccDate(vaccinationDate);
            vaccinationDTO.setVaccType(vaccinationType);
            PetEntity petEntity = petDAO.getPetById(petId);
            VaccinationEntity entity = VaccinationMapper.toEntity(vaccinationDTO, petEntity);
            vaccinationDAO.createEntity(entity);
        }
        
    }
    
    public List<VaccinationDTO> getVaccinationsById(Long petId){
        List<VaccinationDTO> vaccinations = new ArrayList<>();
        vaccinations = VaccinationMapper.toVaccDTOList(vaccinationDAO.getVaccinationsById(petId));
        return vaccinations;
    }
    
}
