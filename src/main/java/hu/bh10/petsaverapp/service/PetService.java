package hu.bh10.petsaverapp.service;

import hu.bh10.petsaverapp.dao.AnnouncerDAO;
import hu.bh10.petsaverapp.dao.FosterDAO;
import hu.bh10.petsaverapp.dao.FoundDAO;
import hu.bh10.petsaverapp.dao.PetDAO;
import hu.bh10.petsaverapp.dao.UserDAO;
import hu.bh10.petsaverapp.dto.PetDTO;
import hu.bh10.petsaverapp.entity.AnnouncerEntity;
import hu.bh10.petsaverapp.entity.FosterEntity;
import hu.bh10.petsaverapp.entity.FoundEntity;
import hu.bh10.petsaverapp.entity.PetEntity;
import hu.bh10.petsaverapp.entity.UserEntity;
import hu.bh10.petsaverapp.mapper.FoundMapper;
import hu.bh10.petsaverapp.mapper.PetMapper;
import hu.bh10.petsaverapp.mapper.UserMapper;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class PetService {

    @Inject
    PetDAO petDAO;
    
    @Inject
    AnnouncerDAO announcerDAO;
    
    @Inject
    UserDAO userDAO;
    
    @Inject
    FosterDAO fosterDAO;

    public PetService() {
    }

    public PetService(PetDAO petDAO, AnnouncerDAO announcerDAO, UserDAO userDAO, FosterDAO fosterDAO) {
        this.petDAO = petDAO;
        this.announcerDAO = announcerDAO;
        this.userDAO = userDAO;
        this.fosterDAO = fosterDAO;
    }
    
    

    public void createPet(PetDTO pet, Long announcerId, String userEmail) {
        AnnouncerEntity announcerEntity = announcerDAO.findAnnouncerById(announcerId);
        UserEntity userEntity = userDAO.getUserByEmail(userEmail);
        PetEntity entity = PetMapper.toEntity(pet, announcerEntity, userEntity, null);       
        petDAO.createEntity(entity);
        
    }
    
//    public void createPet(PetDTO pet, Long announcerId, String userEmail, String fosterEmail) {
//        AnnouncerEntity announcerEntity = announcerDAO.findAnnouncerById(announcerId);
//        UserEntity userEntity = userDAO.getUserByEmail(userEmail);
//        FosterEntity fosterEntity = fosterDAO.getFosterByEmail(fosterEmail);
//        PetEntity entity = PetMapper.toEntity(pet, announcerEntity, userEntity, fosterEntity);       
//        petDAO.createEntity(entity);
//        
//    }
    
    public void updatePet(String nickName, String catOrDog, String age, String color, String sex, String weight, String type, String subType, String dangerous, String isFound, String image, String userEmail, Long petId){
        PetEntity pet = petDAO.getPetById(petId);
        pet.setNickName(nickName);
        if(catOrDog.toUpperCase().equals("TRUE")){
            pet.setCatOrDog("true");
        }else{
            pet.setCatOrDog("false");
        }        
        pet.setAge(Integer.valueOf(age));//itt dobhat NumberFormatException-t.
        pet.setColor(color);
        if(sex.toUpperCase().equals("TRUE")){
            pet.setSex("true");
        }else{
            pet.setSex("false");
        }       
        pet.setWeight(Double.valueOf(weight));//itt dobhat NumberFormatException-t.
        pet.setType(type);
        pet.setSubtype(subType);
        if(dangerous.toUpperCase().equals("TRUE")){
            pet.setDangerous("true");
        }else{
            pet.setDangerous("false");
        }
        pet.setImage(image);
        pet.setFound(isFound);//Mivel megtalált kisállat adatain dolgozunk ezért ennek értéke true;
        pet.setInterestCounter(0);//Ezt kezdetben 0-r állitjuk.
        pet.setSuccess("false");//Ezt kezdetben false-ra állitjuk.
        pet.setHandlerUser(userDAO.getUserByEmail(userEmail));
        petDAO.updatePet(pet);
    }
    
    public Long getMaxPetId() {              
        Long maxId = petDAO.getMaxPetEntityId();
        return maxId;
    }
    
    public PetEntity getPetById(Long petId){
        return petDAO.getPetById(petId);
    }

    public List<PetEntity> findAll() {
        return petDAO.findAll(PetEntity.class);
    }
    
    public List<PetEntity> findPets(boolean foundOrLost) {
        String foundOrLostText = null;
       
        if(foundOrLost){
            foundOrLostText = "true";
        }else{
            foundOrLostText = "false";
        }
         System.out.println("Elveszettt vagy Megtalalat?"+foundOrLostText);
        return petDAO.findFoundOrLostPets(foundOrLostText);
    }
    
    public List<PetEntity> findLostPets() {
        return petDAO.findLostPets();
    }

    public void removePet(PetEntity petEntity, Long id) {
//        petDAO.removePet(petEntity, id);
    }

}
