package hu.bh10.petsaverapp.service;

import java.util.Properties;
import javax.ejb.Singleton;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Singleton
public class EmailSend {

    public void sendingEmail(String invocation,String EmailAddress) {

        final String username = "petsaverapp@gmail.com";
        final String password = "NeedForDeploy";

        Properties prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true"); //TLS

        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("petsaverapp@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(EmailAddress)
            );
            message.setSubject("Üzenet a petsaverapp-tól");
            message.setText("Kedves "+ invocation+"," 
                    + "\n\n Köszönjük Bejelentését!,"
                            + "\n\n Kollégáink hamarosan felveszik Önnel a kapcsolatot!"
                            + "\n\n Üdvözlettel,"
                            + "\n\n A PetSaverApp csapata");

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}

