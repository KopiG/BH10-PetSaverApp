package hu.bh10.petsaverapp.service;

import hu.bh10.petsaverapp.dao.FosterDAO;
import hu.bh10.petsaverapp.dto.FosterDTO;
import hu.bh10.petsaverapp.entity.FosterEntity;
import hu.bh10.petsaverapp.mapper.FosterMapper;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class FosterService {

    @Inject
    FosterDAO fosterDAO;

    public void createFoster(FosterDTO foster) {
//        if(!fosterEmailIsExist(foster.getFosterEmail())){//Ha nem talál ugyanolyan email cimet
            FosterEntity entity = FosterMapper.toEntity(foster);            
            fosterDAO.createEntity(entity);
//        }
    }
    
    public boolean fosterEmailIsExist(String fosterEmail){
        return fosterDAO.fosterEmailIsExist(fosterEmail);
    }
    
    public FosterDTO getFosterByEmail(String fosterEmail){
        FosterDTO foster = FosterMapper.toDTO(fosterDAO.getFosterByEmail(fosterEmail));
        return foster;
    }

}
