package hu.bh10.petsaverapp.dao;

import hu.bh10.petsaverapp.entity.LostEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.transaction.Transactional;

@Stateless
public class LostDAO extends BaseDAO{

    @Transactional
    public void createLost(LostEntity lostEntity) {
        em.persist(lostEntity);
    }

    @Transactional(Transactional.TxType.SUPPORTS)
    public void removeLost(LostEntity lostEntity, Long id) {
        em.createQuery("delete from lost where id=" + lostEntity.getId());
    }
    
    public List<LostEntity> findAllNotAdded(){
        String added = "false";
        List<LostEntity> lostButNotAdded = em.createNamedQuery(LostEntity.NQ_GET_LOST_BUT_NOT_ADDED_PET, LostEntity.class)
                .setParameter(LostEntity.PARAM_ADDED, added)
                .getResultList();
        return lostButNotAdded;
    }
    
    public LostEntity getLostById(Long id){
        return em.createNamedQuery(LostEntity.NQ_GET_LOST_BY_ID, LostEntity.class)
                .setParameter(LostEntity.PARAM_ID, id)
                .getSingleResult();
    }
    
    @Transactional
    public void updateLost(LostEntity lost){
            em.merge(lost);
    }
}
