package hu.bh10.petsaverapp.dao;

import hu.bh10.petsaverapp.entity.FoundEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.transaction.Transactional;

@Stateless
public class FoundDAO extends BaseDAO{

    @Transactional
    public void createFound(FoundEntity foundEntity){
        em.persist(foundEntity);
    }
    
    @Transactional(Transactional.TxType.SUPPORTS)
    public void removeFound(FoundEntity foundEntity, Long id){
         em.createQuery("delete from found where id="+foundEntity.getId());
    }
    
    public List<FoundEntity> findAllNotAdded(){
        String added = "false";
        List<FoundEntity> foundButNotAdded = em.createNamedQuery(FoundEntity.NQ_GET_FOUND_BUT_NOT_ADDED_PET, FoundEntity.class)
                .setParameter(FoundEntity.PARAM_ADDED, added)
                .getResultList();
        return foundButNotAdded;
    }
    
    public FoundEntity getFoundById(Long id){
        return em.createNamedQuery(FoundEntity.NQ_GET_FOUND_BY_ID, FoundEntity.class)
                .setParameter(FoundEntity.PARAM_ID, id)
                .getSingleResult();
    }
    
    @Transactional
    public void updateFound(FoundEntity found){
            em.merge(found);
    }
}
