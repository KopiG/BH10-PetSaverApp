package hu.bh10.petsaverapp.dao;

import hu.bh10.petsaverapp.entity.PetEntity;
import static hu.bh10.petsaverapp.entity.PetEntity.NQ_GET_FOUND_OR_LOST_PETS;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.transaction.Transactional;

@Stateless
public class PetDAO extends BaseDAO{


    @Transactional
    public void createEntity(PetEntity petEntity){
        em.persist(petEntity);
        em.flush();
    }
    
    @Transactional
    public void updatePet(PetEntity petEntity){
        em.merge(petEntity);
    }
    
    public PetEntity getLastPetEntity(){
        return em.createNamedQuery(PetEntity.NQ_GET_LAST_SAVED_PET, PetEntity.class)
                .getSingleResult();
    }
    
    public Long getMaxPetEntityId(){
        Long maxId = em.createNamedQuery(PetEntity.NQ_GET_MAX_PET_ID, Long.class)
                .getSingleResult();
        return maxId;
    }
    
    public PetEntity getPetById(Long petId){
        return em.createNamedQuery(PetEntity.NQ_GET_PET_BY_ID, PetEntity.class)
                .setParameter(PetEntity.PARAM_PET_ID, petId)
                .getSingleResult();
    }
    
    public List<PetEntity> findFoundOrLostPets(String foundOrLost){
        List<PetEntity> foundPets = em.createNamedQuery(PetEntity.NQ_GET_FOUND_OR_LOST_PETS, PetEntity.class)
                .setParameter(PetEntity.PARAM_PET_FOUND_LOST_TYPE, foundOrLost)
                .getResultList();
        return foundPets;
        
    }
    
    public List<PetEntity> findLostPets(){
        List<PetEntity> lostPets = new ArrayList<>();
        
        return lostPets;
        
    }
    
//    @Transactional(Transactional.TxType.SUPPORTS)
//    public void removeAnnouncer(AnnouncerEntity announcerEntity, Long id)
//    {
//        // refaktor
//         em.createQuery("delete from announcer where id="+announcerEntity.getId());
//    }
//    
//    public AnnouncerEntity findAnnouncerById(Long announcerId){
//        return em.createNamedQuery(AnnouncerEntity.NQ_GET_ANNOUNCER_BY_ID, AnnouncerEntity.class)
//                .setParameter(AnnouncerEntity.PARAM_ANNOUNCER_ID, announcerId)
//                .getSingleResult();
//    }
//    
//    public AnnouncerEntity getAnnouncerByEmail(String announcerMail){
//        return em.createNamedQuery(AnnouncerEntity.NQ_GET_ANNOUNCER_BY_MAIL, AnnouncerEntity.class)
//                .setParameter(AnnouncerEntity.PARAM_ANNOUNCER_EMAIL, announcerMail)
//                .getSingleResult();
//    }
//    
//    public Long getAnnouncerIdByEmail(String email){
//        return em.createNamedQuery(AnnouncerEntity.NQ_GET_ANNOUNCER_ID_BY_EMAIL, AnnouncerEntity.class)
//                .setParameter(AnnouncerEntity.PARAM_ANNOUNCER_EMAIL, email)
//                .getSingleResult().getId();
//    }
//    
//    public boolean checkAnnouncerEmail(String email){
//        List list = em.createNamedQuery(AnnouncerEntity.NQ_CHECK_ANNOUNCER_EMAIL, AnnouncerEntity.class)
//                .setParameter(AnnouncerEntity.PARAM_ANNOUNCER_EMAIL, email)
//                .getResultList();
//        // return !isEmpty;
//        if(list.isEmpty()){      
//            return false;
//        }else{
//            return true;
//        }
//    }
}
