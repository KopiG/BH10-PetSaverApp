package hu.bh10.petsaverapp.dao;

import hu.bh10.petsaverapp.entity.AnnouncerEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.transaction.Transactional;

@Stateless
public class AnnouncerDAO extends BaseDAO{


    @Transactional
    public void createEntity(AnnouncerEntity announcerEntity){
        em.persist(announcerEntity);
        em.flush();
        
    }
    
    @Transactional
    public void updateAnnouncer(AnnouncerEntity announcerEntity){
        em.merge(announcerEntity);
        
    }
    
    @Transactional(Transactional.TxType.SUPPORTS)
    public void removeAnnouncer(AnnouncerEntity announcerEntity, Long id)
    {
        // refaktor
         em.createQuery("delete from announcer where id="+announcerEntity.getId());
    }
    
    public AnnouncerEntity findAnnouncerById(Long announcerId){
        return em.createNamedQuery(AnnouncerEntity.NQ_GET_ANNOUNCER_BY_ID, AnnouncerEntity.class)
                .setParameter(AnnouncerEntity.PARAM_ANNOUNCER_ID, announcerId)
                .getSingleResult();
    }
    
    public AnnouncerEntity getAnnouncerByEmail(String announcerMail){
        return em.createNamedQuery(AnnouncerEntity.NQ_GET_ANNOUNCER_BY_MAIL, AnnouncerEntity.class)
                .setParameter(AnnouncerEntity.PARAM_ANNOUNCER_EMAIL, announcerMail)
                .getSingleResult();
    }
    
    public Long getAnnouncerIdByEmail(String email){
        return em.createNamedQuery(AnnouncerEntity.NQ_GET_ANNOUNCER_ID_BY_EMAIL, AnnouncerEntity.class)
                .setParameter(AnnouncerEntity.PARAM_ANNOUNCER_EMAIL, email)
                .getSingleResult().getId();
    }
    
    public boolean checkAnnouncerEmail(String email){
        List list = em.createNamedQuery(AnnouncerEntity.NQ_CHECK_ANNOUNCER_EMAIL, AnnouncerEntity.class)
                .setParameter(AnnouncerEntity.PARAM_ANNOUNCER_EMAIL, email)
                .getResultList();
        // return !isEmpty;
        if(list.isEmpty()){      
            return false;
        }else{
            return true;
        }
    }
}
