package hu.bh10.petsaverapp.dao;

import hu.bh10.petsaverapp.entity.P_UserEntity;
import hu.bh10.petsaverapp.entity.UserEntity;
import hu.bh10.petsaverapp.entity.UserGroup;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;

@Stateless
public class AdminDAO extends BaseDAO{
    
    public void setUsers(List<UserEntity> userList, List<P_UserEntity> pUserList){
        boolean sameEmail = false;
        for (int i = 0; i < pUserList.size(); i++) {
            System.out.println("P_U-S_E-R: "+pUserList.get(i));
        }
        for (int i = 0; i < userList.size(); i++) {
            if(userList.get(i).getValid().equals("no")){
                if(isUserExistInPUserList(userList.get(i).getUserEmail(), pUserList)){
                    removeUser(userList.get(i).getEmail());
                }
                
            }
            if(userList.get(i).getValid().equals("yes")){
                if(!isUserExistInPUserList(userList.get(i).getUserEmail(), pUserList)){
                    P_UserEntity pUser = new P_UserEntity();
                        pUser.setUserEmail(userList.get(i).getUserEmail());
                        pUser.setNickName(userList.get(i).getNickName());
                        pUser.setUserFirstName(userList.get(i).getUserFirstName());
                        pUser.setUserLastName(userList.get(i).getUserLastName());
                        pUser.setUserPassword(userList.get(i).getUserPassword());
                        System.out.println("PUSER: "+pUser);
                        refreshUser(pUser);
                } 
            }
            em.merge(userList.get(i));
        }
    }
    public boolean isUserExistInPUserList(String userEmail, List<P_UserEntity> pUserList){
        for (int i = 0; i < pUserList.size(); i++) {
            if(userEmail.equals(pUserList.get(i).getUserEmail()))
                return true;
        }
        return false;
    }
    
    public void removeUser(String userEmail){
        P_UserEntity pEntity = em.find(P_UserEntity.class, userEmail);
        em.remove(pEntity);
    }
    
    public void refreshUser(P_UserEntity user){
        em.persist(user);
    }
    
    public List<UserEntity> getUsers(){
        List<UserEntity> userList = new ArrayList();
        userList =  em.createNamedQuery(UserEntity.NQ_GET_USERS, UserEntity.class)
                .getResultList();
        return userList;
    }
    
    public List<P_UserEntity> getPUsers(){
        List<P_UserEntity> puserList = new ArrayList();
        puserList =  em.createNamedQuery(P_UserEntity.NQ_GET_P_USERS, P_UserEntity.class)
                .getResultList();
        return puserList;
    }
    
    public List<UserGroup> getGroups(String email){
        List<UserGroup> groupList = new ArrayList();
        groupList = em.createNamedQuery(UserGroup.NQ_GET_USER_GROUPS_BY_EMAIL, UserGroup.class)
                .setParameter(UserGroup.PARAM_USER_EMAIL, email)
                .getResultList();
        return groupList;
    }
    
}
