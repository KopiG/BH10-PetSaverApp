package hu.bh10.petsaverapp.dao;

import hu.bh10.petsaverapp.entity.RescueCircumstanceEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.transaction.Transactional;

@Stateless
public class RescueCircumstanceDAO extends BaseDAO{
    
    @Transactional
    public void createEntity(RescueCircumstanceEntity rescueEntity){
        em.persist(rescueEntity);
    }
    
    public List<RescueCircumstanceEntity> getRescuesById(Long petId){
        return em.createNamedQuery(RescueCircumstanceEntity.NQ_GET_RESCUES_BY_ID, RescueCircumstanceEntity.class)
                .setParameter(RescueCircumstanceEntity.PARAM_PET_ID, petId)
                .getResultList();
    }
    
    public RescueCircumstanceEntity getRescueById(Long rescueId){
        return em.createNamedQuery(RescueCircumstanceEntity.NQ_GET_RESCUE_BY_ID, RescueCircumstanceEntity.class)
                .setParameter(RescueCircumstanceEntity.PARAM_RESCUE_ID, rescueId)
                .getSingleResult();
    }
    
     @Transactional
    public void updateEntity(RescueCircumstanceEntity rescueEntity){
        em.merge(rescueEntity);
    }
    
}
