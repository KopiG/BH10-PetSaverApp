package hu.bh10.petsaverapp.dao;

import hu.bh10.petsaverapp.entity.FosterEntity;
import javax.ejb.Stateless;
import javax.transaction.Transactional;

@Stateless
public class FosterDAO extends BaseDAO{


    @Transactional
    public void createEntity(FosterEntity fosterEntity){
        em.persist(fosterEntity);
        em.flush();
        
    }
    
    public FosterEntity getFosterByEmail(String email) {
        return em.createNamedQuery(FosterEntity.NQ_GET_FOSTER_BY_EMAIL, FosterEntity.class)
                .setParameter(FosterEntity.PARAM_FOSTER_EMAIL, email)
                .getSingleResult();
    }
    
    public boolean fosterEmailIsExist(String email) {
        
        int emailNumber =  em.createNamedQuery(FosterEntity.NQ_CHECK_FOSTER_BY_EMAIL, Integer.class)
                .setParameter(FosterEntity.PARAM_FOSTER_EMAIL, email)
                .getSingleResult();
        return emailNumber > 0;
    }

}
