package hu.bh10.petsaverapp.servlet;

import hu.bh10.petsaverapp.dto.FoundDTO;
import hu.bh10.petsaverapp.dto.LostDTO;
import hu.bh10.petsaverapp.entity.FoundEntity;
import hu.bh10.petsaverapp.entity.LostEntity;
import hu.bh10.petsaverapp.mapper.FoundMapper;
import hu.bh10.petsaverapp.mapper.LostMapper;
import hu.bh10.petsaverapp.service.FoundFormService;
import hu.bh10.petsaverapp.service.LostFormService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.security.DeclareRoles;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "userPageServlet", urlPatterns = {"/userPageServlet"})
@DeclareRoles({"admin", "user"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"admin", "user"}))
public class UserPageServlet extends HttpServlet {
    
    @Inject
    private FoundFormService foundFormService;
    
    @Inject
    private LostFormService lostFormService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        List<FoundEntity> entities = foundFormService.findAllNotAdded();
        List<FoundDTO> foundPets = new ArrayList<>();
        for (int i = 0; i < entities.size(); i++) {
            FoundDTO pet = FoundMapper.toFoundDTO(entities.get(i));
            foundPets.add(pet);
        }
        session.setAttribute("foundPets", foundPets);
        
        List<LostEntity> entitiesLostPet = lostFormService.findAllNotAdded();
        List<LostDTO> lostPets = new ArrayList<>();
        for (int i = 0; i < entitiesLostPet.size(); i++) {
            LostDTO pets = LostMapper.toLostDTO(entitiesLostPet.get(i));
            lostPets.add(pets);
        }
        session.setAttribute("lostPets", lostPets);
        
        request.getRequestDispatcher("WEB-INF/userPage.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        response.sendRedirect("userPageServlet");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
