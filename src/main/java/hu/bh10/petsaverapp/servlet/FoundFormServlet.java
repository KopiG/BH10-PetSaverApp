package hu.bh10.petsaverapp.servlet;

import hu.bh10.petsaverapp.dto.AnnouncerDTO;
import hu.bh10.petsaverapp.dto.FoundDTO;
import hu.bh10.petsaverapp.service.AnnouncerService;
import hu.bh10.petsaverapp.service.EmailSend;
import hu.bh10.petsaverapp.service.FoundFormService;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.annotation.security.DeclareRoles;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/foundFormServlet"})
@DeclareRoles({"admin", "user"})
public class FoundFormServlet extends HttpServlet {

    @Inject
    private FoundFormService foundFormService;

    @Inject
    private AnnouncerService announcerService;

    @Inject
    private EmailSend emailSend;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("WEB-INF/homePage.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        String foundPlace = (String) request.getParameter("foundPlace");
        String sex = (String) request.getParameter("sex");
        String petProperties = (String) request.getParameter("petProperties");

        //String to LocalDate
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate foundDate = LocalDate.parse(request.getParameter("foundDate"), dtf);

        String image = (String) request.getParameter("image");
        String announcerName = (String) request.getParameter("foundName");
        String announcerPhone = (String) request.getParameter("foundPhone");
        String announcerMail = (String) request.getParameter("foundMail");

        AnnouncerDTO freshAnnouncer = new AnnouncerDTO();
        freshAnnouncer.setNameOfAnnouncer(announcerName);
        freshAnnouncer.setPhoneOfAnnouncer(announcerPhone);
        freshAnnouncer.setEmailOfAnnouncer(announcerMail);
        announcerService.createAnnouncer(freshAnnouncer);

        AnnouncerDTO savedAnnouncer = announcerService.getAnnouncerByEmail(announcerMail);

        FoundDTO found = new FoundDTO();
        found.setFoundPlace(foundPlace);
        found.setSex(sex);
        found.setPetProperties(petProperties);
        found.setFoundDate(foundDate);
        found.setImage(image);
        found.setAdded(false);
        found.setAnnouncer(savedAnnouncer);
        Long savedAnnouncerId = Long.valueOf(savedAnnouncer.getAnnouncerId());

        foundFormService.createFound(found, savedAnnouncerId);

        emailSend.sendingEmail(announcerName, announcerMail);
        
        request.getRequestDispatcher("WEB-INF/successfullNotification.jsp").include(request, response);
//        response.sendRedirect("homePageServlet");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
