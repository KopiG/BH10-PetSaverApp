package hu.bh10.petsaverapp.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.annotation.security.DeclareRoles;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "foundPetUpdate", urlPatterns = {"/foundPetUpdateServlet"})
@DeclareRoles({"admin", "user"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"admin", "user"}))
public class FoundPetUpdateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            String id = request.getParameter("id");
            String catOrDog = request.getParameter("catOrDog");
            String age = request.getParameter("age");
            String color = request.getParameter("color");
            System.out.println("Atjovunk ide a foundPetUpdateServlet-re?" +id);
            request.getRequestDispatcher("WEB-INF/updatePetData.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
