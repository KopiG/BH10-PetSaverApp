package hu.bh10.petsaverapp.servlet;

import hu.bh10.petsaverapp.dto.FosterDTO;
import hu.bh10.petsaverapp.service.FosterService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "FosterRegistrationServlet", urlPatterns = {"/fosterRegistrationServlet"})
public class FosterRegistrationServlet extends HttpServlet {

    @Inject
    FosterService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("WEB-INF/fosterRegistration.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String lastName = request.getParameter("lastName");
        String firstName = request.getParameter("firstName");
        String note = request.getParameter("note");

        PrintWriter out = response.getWriter();

        FosterDTO foster = new FosterDTO();
//        boolean isExist = false;
//        try {
//            isExist = service.fosterEmailIsExist(email);
//        } catch (Exception ex) {
//            Logger.getLogger(RegistrationServlet.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        if (isExist) {
//            out.println("Örökbefogadó " + email + " email cimmel már létezik.");
//        } else {
            foster.setFosterFirstName(firstName);
            foster.setFosterLastName(lastName);
            foster.setFosterEmail(email);
            foster.setFosterPhone(phone);
            foster.setFosterNote(note);
            service.createFoster(foster);

            request.getRequestDispatcher("WEB-INF/successfullFosterRegistration.jsp").forward(request, response);
//        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
