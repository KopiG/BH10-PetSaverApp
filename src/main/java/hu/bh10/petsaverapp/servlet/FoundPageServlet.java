package hu.bh10.petsaverapp.servlet;

import hu.bh10.petsaverapp.dto.AnnouncerDTO;
import hu.bh10.petsaverapp.dto.ChipDTO;
import hu.bh10.petsaverapp.dto.FosterDTO;
import hu.bh10.petsaverapp.dto.FoundDTO;
import hu.bh10.petsaverapp.dto.HealthDiagnoseDTO;
import hu.bh10.petsaverapp.dto.PetDTO;
import hu.bh10.petsaverapp.dto.RescueCircumstanceDTO;
import hu.bh10.petsaverapp.dto.UserDTO;
import hu.bh10.petsaverapp.dto.VaccinationDTO;
import hu.bh10.petsaverapp.entity.AnnouncerEntity;
import hu.bh10.petsaverapp.entity.FoundEntity;
import hu.bh10.petsaverapp.entity.PetEntity;
import hu.bh10.petsaverapp.mapper.AnnouncerMapper;
import hu.bh10.petsaverapp.mapper.FoundMapper;
import hu.bh10.petsaverapp.mapper.PetMapper;
import hu.bh10.petsaverapp.service.AnnouncerService;
import hu.bh10.petsaverapp.service.ChipService;
import hu.bh10.petsaverapp.service.FosterService;
import hu.bh10.petsaverapp.service.FoundFormService;
import hu.bh10.petsaverapp.service.HealthDiagnoseService;
import hu.bh10.petsaverapp.service.PetService;
import hu.bh10.petsaverapp.service.RescueCircumstanceService;
import hu.bh10.petsaverapp.service.UserService;
import hu.bh10.petsaverapp.service.VaccinationService;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.security.DeclareRoles;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = {"/foundPageServlet"})
@DeclareRoles({"admin", "user"})
public class FoundPageServlet extends HttpServlet {
    
    @Inject
    private UserService userService;
    
    @Inject
    private AnnouncerService announcerService;
    
    @Inject
    private FosterService fosterService;
    
    @Inject
    private PetService petService;
    
    @Inject
    private RescueCircumstanceService rescueCircumstanceService;
    
    @Inject
    private ChipService chipService;
    
    @Inject
    private HealthDiagnoseService healthDiagnoseService;
    
    @Inject
    private VaccinationService vaccinationService;
    
    @Inject
    private FoundFormService foundFormService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //Itt kell a DB-ből kivenni a rekordokat és feltölteni egy listát
        HttpSession session = request.getSession();
        List<PetEntity> entities = petService.findPets(true);//true = foundPets
        List<PetDTO> foundPetsList = new ArrayList<>();
        for (int i = 0; i < entities.size(); i++) {
            PetDTO pet = PetMapper.toPetDTO(entities.get(i));
            foundPetsList.add(pet);
        }
        session.setAttribute("foundPetsList", foundPetsList);
        request.getRequestDispatcher("WEB-INF/foundPage.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.setCharacterEncoding("UTF-8");
        
        //Itt elkérni az adatokat az updateFoundPetPage.jsp-től.
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        String nickName = (String)request.getParameter("nickName");//Kötelező
        String catOrDog = (String)request.getParameter("catOrDog");//Kötelező
        String age = (String)request.getParameter("age");//Kötelező
        String color = (String)request.getParameter("color");//Kötelező
        String sex = (String)request.getParameter("sex");//Kötelező
        String weight = (String)request.getParameter("weight");
        String type = (String)request.getParameter("type");
        String subType = (String)request.getParameter("subType");
        String dangerous = request.getParameter("dangerous");
        String imageURL = (String)request.getParameter("image");//Kötelező
        
        //Összeállitjuk a PetDTO-t (meg kell jelölni, hogy lost vagy found - akár egy plusz mezővel a DTO-ban.)
        PetDTO freshPet = new PetDTO();
        freshPet.setNickName(nickName);
        if(catOrDog.toUpperCase().equals("TRUE")){
            freshPet.setCatOrDog(true);
        }else{
            freshPet.setCatOrDog(false);
        }        
        freshPet.setAge(Integer.valueOf(age));//itt dobhat NumberFormatException-t.
        freshPet.setColor(color);
        if(sex.toUpperCase().equals("TRUE")){
            freshPet.setSex(true);
        }else{
            freshPet.setSex(false);
        }       
        freshPet.setWeight(Double.valueOf(weight));//itt dobhat NumberFormatException-t.
        freshPet.setType(type);
        freshPet.setSubType(subType);
        if(dangerous.toUpperCase().equals("TRUE")){
            freshPet.setDangerous(true);
        }else{
            freshPet.setDangerous(false);
        }
        freshPet.setImage(imageURL);
        freshPet.setFound(true);//Mivel megtalált kisállat adatain dolgozunk ezért ennek értéke true;
        freshPet.setInterestCounter(0);//Ezt kezdetben 0-r állitjuk.
        freshPet.setSuccess(false);//Ezt kezdetben false-ra állitjuk.

        //Megszerezzük a User objektumot
        String userEmail = (String)request.getParameter("userEmail");//Kötelező       
        UserDTO userDTO = userService.getUserByEmail(userEmail);
        freshPet.setUser(userDTO);
        
        //Megszerezzök az Announcer objektumot
        String announcerid = (String)request.getParameter("announcerid");//Kötelező
        Long announcerId = Long.valueOf(announcerid);
        AnnouncerDTO announcerDTO = announcerService.getAnnouncerById(announcerId);
        freshPet.setAnnouncer(announcerDTO);
        
        //Megszerezzök a Foster objektumot - egyenlőre null, mert Foster itt még nem létezik.
//        String fosterEmail = (String)request.getParameter("fosterEmail");
//        String fosterEmail = "foster@gmail.com";
//        FosterDTO fosterDTO = fosterService.getFosterByEmail(fosterEmail);
//        System.out.println("Foster: "+fosterDTO);
//        freshPet.setFoster(fosterDTO);
        
        //Itt elmentjük a PetDTO-t a DB-be.
//        petService.createPet(freshPet, announcerId, userEmail, fosterEmail);
        petService.createPet(freshPet, announcerId, userEmail);
        
        
        //Itt vissza kell kérni a frissen mentett objektum id-ját, hogy az id alapján elkért Pet objektumot beletehessük a 4 db mellék táblába.
        Long petId = petService.getMaxPetId();
        
        PetEntity savedPet = petService.getPetById(petId);
        PetDTO savedPetDTO = PetMapper.toPetDTO(savedPet);
        
        //RescueCircumstance objektum létrehozása a Pet objektummal.
        String foundPlace = (String)request.getParameter("foundPlace");//Kötelező
        LocalDate foundDate = LocalDate.parse(request.getParameter("foundDate"), dtf);//Kötelező
        String foundNote = (String)request.getParameter("foundNote");
        RescueCircumstanceDTO rescueDTO = new RescueCircumstanceDTO();
        rescueDTO.setRescueDate(foundDate);
        rescueDTO.setRescuePlace(foundPlace);
        rescueDTO.setRescueNote(foundNote);
        rescueCircumstanceService.createRescueCircumstance(rescueDTO, petId);
        
        
        //Chip objektum létrehozása a Pet objektummal.
        String owned = (String)request.getParameter("owned");
        String chipNumber = (String)request.getParameter("chipNumber");
        LocalDate chipDate = LocalDate.parse(request.getParameter("chipDate"), dtf);
        ChipDTO chipDTO = new ChipDTO();
        chipDTO.setChipNumber(chipNumber);
        chipDTO.setChipDate(chipDate);
        chipService.createChip(chipDTO, petId);
        
        
        //HeathDiagnose objektum létrehozása a Pet objektummal.
        LocalDate healthDiagnoseDate = LocalDate.parse(request.getParameter("healthDiagnoseDate"), dtf);
        String healthDiagnoseCause = (String)request.getParameter("healthDiagnoseCause");
        String healthDiagnoseResult = (String)request.getParameter("healthDiagnoseResult");
        HealthDiagnoseDTO healthDiagnoseDTO = new HealthDiagnoseDTO();
        healthDiagnoseDTO.setDiagDate(healthDiagnoseDate);
        healthDiagnoseDTO.setCause(healthDiagnoseCause);
        healthDiagnoseDTO.setResult(healthDiagnoseResult);
        healthDiagnoseService.createHealthDiagnose(healthDiagnoseDTO, petId);
        
        //Vaccination objektum létrehozása a petId-val.
        LocalDate vaccinationDate = LocalDate.parse(request.getParameter("vaccinationDate"), dtf);
        String vaccinationType = (String)request.getParameter("vaccinationType");
        VaccinationDTO vaccinationDTO = new VaccinationDTO();
        vaccinationDTO.setVaccDate(vaccinationDate);
        vaccinationDTO.setVaccType(vaccinationType);
        vaccinationService.createVaccination(vaccinationDTO, petId);       
        
         //Itt vesszük le a user page gyorsbejelentő listáról a bejelentett megtalált kisállatot.(Az added értékét true-ra állitjuk)
        String foundPetId = (String)request.getParameter("petId");
        foundFormService.updateFound(Long.valueOf(foundPetId));
        
        //Itt kell a DB-ből kivenni a rekordokat és feltölteni egy listát
        HttpSession session = request.getSession();
        List<PetEntity> entities = petService.findPets(savedPetDTO.isFound());
        List<PetDTO> foundPetsList = new ArrayList<>();
        for (int i = 0; i < entities.size(); i++) {
            PetDTO pet = PetMapper.toPetDTO(entities.get(i));
            System.out.println("PetDTO: "+pet);
            foundPetsList.add(pet);
        }
        session.setAttribute("foundPetsList", foundPetsList);

        //Itt kell átküldeni a listát a foundPage.jsp-nek.
        request.getRequestDispatcher("WEB-INF/foundPage.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
