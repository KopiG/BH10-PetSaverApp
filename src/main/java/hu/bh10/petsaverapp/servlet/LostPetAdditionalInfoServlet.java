package hu.bh10.petsaverapp.servlet;

import hu.bh10.petsaverapp.service.PetService;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(urlPatterns = {"/lostPetAdditionalInfoServlet"})
public class LostPetAdditionalInfoServlet extends HttpServlet {
    
   

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("WEB-INF/lostPetAdditionalInfoPage.jsp").forward(request, response);
         
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
          request.setCharacterEncoding("UTF-8");
        
   
      
        response.sendRedirect("lostPetAdditionalInfoServlet");
    }

}
