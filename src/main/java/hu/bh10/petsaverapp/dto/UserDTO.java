package hu.bh10.petsaverapp.dto;

import java.util.List;

public class UserDTO {
    private String nickName;
    private String userEmail;
    private String userFirstName;
    private String userLastName;      
    private String userPassword;     
    private String role;   
    private boolean valid;
    private List<UserGroupDTO> groups;
    private List<PetDTO> pets;

    public UserDTO() {
    }

    public UserDTO(String nickName, String userEmail, String userFirstName, String userLastName, String userPassword,  String role, boolean valid) {
        this.nickName = nickName;
        this.userEmail = userEmail;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.userPassword = userPassword;        
        this.role = role;
        this.valid = valid;
    }
    

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public List<UserGroupDTO> getGroups() {
        return groups;
    }

    public void setGroups(List<UserGroupDTO> groups) {
        this.groups = groups;
    }


    public List<PetDTO> getPets() {
        return pets;
    }

    public void setPets(List<PetDTO> pets) {
        this.pets = pets;
    }
    
    

    @Override
    public String toString() {
        return "UserDTO{" + "userLastName=" + userLastName + ", userFirstName=" + userFirstName + ", nickName=" + nickName + ", userPassword=" + userPassword + ", userEmail=" + userEmail + ", role=" + role + ", valid=" + valid + '}';
    }
    
    
}
