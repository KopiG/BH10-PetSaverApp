package hu.bh10.petsaverapp.dto;

import java.time.LocalDate;

public class ChipDTO {
    
    private Long id;
    private String chipNumber;
    private LocalDate chipDate;
    private String vetName;
    private PetDTO pet;

    public ChipDTO() {
    }

    public ChipDTO(Long id, String chipNumber, LocalDate chipDate, String vetName, PetDTO pet) {
        this.id = id;
        this.chipNumber = chipNumber;
        this.chipDate = chipDate;
        this.vetName = vetName;
        this.pet = pet;
    }

    public ChipDTO(String chipNumber, PetDTO pet) {
        this.id = id;
        this.chipNumber = chipNumber;
        this.pet = pet;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getChipNumber() {
        return chipNumber;
    }

    public void setChipNumber(String chipNumber) {
        this.chipNumber = chipNumber;
    }

    public LocalDate getChipDate() {
        return chipDate;
    }

    public void setChipDate(LocalDate chipDate) {
        this.chipDate = chipDate;
    }

    public String getVetName() {
        return vetName;
    }

    public void setVetName(String vetName) {
        this.vetName = vetName;
    }

    public PetDTO getPet() {
        return pet;
    }

    public void setPet(PetDTO pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "ChipDTO{" + "chipNumber=" + chipNumber + ", chipDate=" + chipDate + ", vetName=" + vetName + ", pet=" + pet + '}';
    }

    
}
