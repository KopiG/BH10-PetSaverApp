package hu.bh10.petsaverapp.dto;

import java.time.LocalDate;

public class HealthDiagnoseDTO {
    
    public static final String NEUTRED = "neutred";
    public static final String WOUNDED = "wounded";
    
    private Long id;
    private LocalDate diagDate;
    private String cause;
    private String result;
    private String medicine;
    private String vetName;
    private PetDTO pet;


    public HealthDiagnoseDTO() {
    }

    public HealthDiagnoseDTO(Long id, LocalDate diagDate, String cause, String result, String medicine, String vetName, PetDTO pet) {
        this.id = id;
        this.diagDate = diagDate;
        this.cause = cause;
        this.result = result;
        this.medicine = medicine;
        this.vetName = vetName;
        this.pet = pet;
    }

    public HealthDiagnoseDTO(LocalDate diagDate, String vetName, PetDTO pet) {
        this.id = id;
        this.diagDate = diagDate;
        this.vetName = vetName;
        this.pet = pet;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public LocalDate getDiagDate() {
        return diagDate;
    }

    public void setDiagDate(LocalDate diagDate) {
        this.diagDate = diagDate;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMedicine() {
        return medicine;
    }

    public void setMedicine(String medicine) {
        this.medicine = medicine;
    }

    public String getVetName() {
        return vetName;
    }

    public void setVetName(String vetName) {
        this.vetName = vetName;
    }

    public PetDTO getPet() {
        return pet;
    }

    public void setPet(PetDTO pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "HealthDiagnoseDTO{" + "diagDate=" + diagDate + ", cause=" + cause + ", result=" + result + ", medicine=" + medicine + ", vetName=" + vetName + ", pet=" + pet + '}';
    }

    

}
