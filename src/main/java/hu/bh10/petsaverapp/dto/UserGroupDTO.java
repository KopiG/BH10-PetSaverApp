package hu.bh10.petsaverapp.dto;

import java.util.List;

public class UserGroupDTO {
    
    private String groupName;
    private List<UserDTO> userGroups;

    public UserGroupDTO() {
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<UserDTO> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(List<UserDTO> userGroups) {
        this.userGroups = userGroups;
    }

    @Override
    public String toString() {
        return "UserGroupDTO{" + "groupName=" + groupName + ", userGroups=" + userGroups + '}';
    }
    
    
    
}
