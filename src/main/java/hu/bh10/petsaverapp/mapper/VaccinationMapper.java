package hu.bh10.petsaverapp.mapper;

import hu.bh10.petsaverapp.dto.PetDTO;
import hu.bh10.petsaverapp.dto.VaccinationDTO;
import hu.bh10.petsaverapp.entity.PetEntity;
import hu.bh10.petsaverapp.entity.VaccinationEntity;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class VaccinationMapper {
    
    public VaccinationMapper() {
    }
    
    public static VaccinationEntity toEntity(VaccinationDTO dto, PetEntity pet) {
        VaccinationEntity entity = new VaccinationEntity();

        Date vaccDate = Date.valueOf(dto.getVaccDate());
        entity.setVaccDate(vaccDate);
        entity.setVaccType(dto.getVaccType());
        entity.setVaccinatedPet(pet);

        return entity;
    }

    public static VaccinationDTO toDTO(VaccinationEntity entity, PetDTO petDTO) {

        VaccinationDTO dto = new VaccinationDTO();
        
        LocalDate dateFromEntity = entity.getVaccDate().toLocalDate();
        dto.setVaccDate(dateFromEntity);
        dto.setVaccType(entity.getVaccType());
        dto.setPet(petDTO);

        return dto;
    }
    
    public static VaccinationDTO toVaccDTO(VaccinationEntity entity) {

        VaccinationDTO dto = new VaccinationDTO();
        dto.setId(entity.getId());
        LocalDate dateFromEntity = entity.getVaccDate().toLocalDate();
        dto.setVaccDate(dateFromEntity);
        dto.setVaccType(entity.getVaccType());
        dto.setPet(PetMapper.toPetDTO(entity.getVaccinatedPet()));

        return dto;
    }
    
    public static VaccinationEntity toVaccEntity(VaccinationDTO dto){
        
        VaccinationEntity entity = new VaccinationEntity();
        Date vaccDate = Date.valueOf(dto.getVaccDate());
        entity.setVaccDate(vaccDate);
        entity.setVaccType(dto.getVaccType());
        entity.setVaccinatedPet(PetMapper.toPetEntity(dto.getPet()));
        
        return entity;
        
    }
    
    public static List<VaccinationDTO> toVaccDTOList(List<VaccinationEntity> entities){
        List<VaccinationDTO> dtos = new ArrayList<>();
        for (int i = 0; i < entities.size(); i++) {
            VaccinationDTO dto = VaccinationMapper.toVaccDTO(entities.get(i));
            dtos.add(dto);
        }
        return dtos;
    }
    
    public static List<VaccinationEntity> toVaccEntityList(List<VaccinationDTO> dtos){
        List<VaccinationEntity> entities = new ArrayList<>();
        for (int i = 0; i < dtos.size(); i++) {
            VaccinationEntity entity = VaccinationMapper.toVaccEntity(dtos.get(i));
            entities.add(entity);
        }
        return entities;
    }
}
