package hu.bh10.petsaverapp.mapper;

import hu.bh10.petsaverapp.dto.AnnouncerDTO;
import hu.bh10.petsaverapp.entity.AnnouncerEntity;
import java.util.ArrayList;
import java.util.List;
  
public class AnnouncerMapper {

    public AnnouncerMapper() {
    } 
    
    public static AnnouncerEntity toAnnouncerEntity(AnnouncerDTO announcerDTO) {
        AnnouncerEntity entity = new AnnouncerEntity();
        entity.setNameOfAnnouncer(announcerDTO.getNameOfAnnouncer());
        entity.setPhoneOfAnnouncer(announcerDTO.getPhoneOfAnnouncer());
        entity.setEmailOfAnnouncer(announcerDTO.getEmailOfAnnouncer());

        return entity;
    }

    public static AnnouncerDTO toAnnouncerDTO(AnnouncerEntity announcerEntity) {

        AnnouncerDTO dto = new AnnouncerDTO();
        dto.setAnnouncerId(announcerEntity.getId());
        dto.setNameOfAnnouncer(announcerEntity.getNameOfAnnouncer());
        dto.setPhoneOfAnnouncer(announcerEntity.getPhoneOfAnnouncer());
        dto.setEmailOfAnnouncer(announcerEntity.getEmailOfAnnouncer());

        return dto;

    }
    
    
    public static List<AnnouncerDTO> toAnnouncerDTO(List<AnnouncerEntity> announcerEntities){
        List<AnnouncerDTO> announcerDTOList = new ArrayList<>();
//        announcerEntities.forEach(announcer -> announcerDTOList.add(toAnnouncerDTO(announcer)));
        for (int i = 0; i < announcerEntities.size(); i++) {
            AnnouncerDTO announcerDTO = new AnnouncerDTO();
            announcerDTO.setNameOfAnnouncer(announcerEntities.get(i).getNameOfAnnouncer());
            announcerDTO.setPhoneOfAnnouncer(announcerEntities.get(i).getPhoneOfAnnouncer());
            announcerDTO.setEmailOfAnnouncer(announcerEntities.get(i).getEmailOfAnnouncer());
            announcerDTOList.add(announcerDTO);
        }
        return announcerDTOList;
             
    }
}
