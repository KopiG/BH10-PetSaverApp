package hu.bh10.petsaverapp.mapper;

import hu.bh10.petsaverapp.dto.UserDTO;
import hu.bh10.petsaverapp.entity.UserEntity;
import hu.bh10.petsaverapp.entity.UserGroup;
import java.util.ArrayList;
import java.util.List;

public class UserMapper {
    
    public static UserEntity toEntity(String nickName, String role, String email, String firstName, String lastName, String password, boolean valid){
        UserEntity userEntity = new UserEntity();
        
        userEntity.setNickName(nickName);
        userEntity.setRole(role);
        userEntity.setUserEmail(email);
        userEntity.setUserFirstName(firstName);
        userEntity.setUserLastName(lastName);
        userEntity.setUserPassword(password);
        if(!valid){
            userEntity.setValid("no");
        }else{
            userEntity.setValid("yes");
        }
        return userEntity;
    }
    
    public static UserEntity toUserEntity(UserDTO dto){
        UserEntity entity = new UserEntity();
        
        entity.setNickName(dto.getNickName());
        entity.setUserEmail(dto.getUserEmail());
        entity.setUserFirstName(dto.getUserFirstName());
        entity.setUserLastName(dto.getUserLastName());
        entity.setUserPassword(dto.getUserPassword());
        entity.setRole(dto.getRole());
        if(!dto.isValid()){
            entity.setValid("no");
        }else{
            entity.setValid("yes");
        }
        //entity.setGroups(UserGroupMapper.toUserGroupEntityList(dto.getGroups()));
        //entity.setPets(PetMapper.toPetListEntity(dto.getPets()));
        
        return entity;
    }
    
    public static UserDTO toUserDTO(UserEntity user){
        UserDTO userDTO = new UserDTO();
        
        userDTO.setNickName(user.getNickName());
        userDTO.setUserEmail(user.getEmail());
        userDTO.setUserFirstName(user.getUserFirstName());
        userDTO.setUserLastName(user.getUserLastName());
        userDTO.setUserPassword(user.getUserPassword());
        userDTO.setRole(user.getRole());
        if(user.getValid().equals("yes")){
            userDTO.setValid(true);
        }else{
            userDTO.setValid(false);
        }
        //userDTO.setGroups(UserGroupMapper.toUserGroupDTOList(user.getGroups()));
        //userDTO.setPets(PetMapper.toPetListDTO(user.getPets()));
        return userDTO;
    }
    
    public static List<UserDTO> toUserDTOList(List<UserEntity> users){
        List<UserDTO> userDTOList = new ArrayList<>();
        for (int i = 0; i < users.size(); i++) {
            UserDTO userDTO = UserMapper.toUserDTO(users.get(i));
            userDTOList.add(userDTO);
        }       
        return userDTOList;
    }
    
    public static List<UserEntity> toUserEntityList(List<UserDTO> users){
        List<UserEntity> entities = new ArrayList<>();
        for (int i = 0; i < users.size(); i++) {
            UserEntity entity = UserMapper.toUserEntity(users.get(i));
            entities.add(entity);
        }      
        return entities;
    }
    
//    public static List<String> entityUserGroupListToStringList(List<UserGroup> groups){
//        List<String> groupList = new ArrayList<>();
//        
//        for (int i = 0; i < groups.size(); i++) {
//            groupList.add(groups.get(i).getGroupName());
//        }
//        return groupList;
//    }
    
}
