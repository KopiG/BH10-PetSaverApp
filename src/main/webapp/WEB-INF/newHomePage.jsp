<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import = "java.io.*,java.util.*" %>
<html>
    <head>
        <title>PetSaver</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Amatic+SC">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css">
        
        <style>
        body, html {height: 100%}
        body,h1,h2,h3,h4,h5,h6 {font-family: "Amatic SC", sans-serif}
        .rapid {display: none}

        .hero-image {
            background-image: url("resources/media/funnyCats.jpg");
            background-color: #cccccc;
            height: 1000px;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            position: relative;
        }

        .hero-text {
            text-align: center;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            color: white;
        }

        /*login button*/
        /* Button used to open the contact form - fixed at the bottom of the page */
        .open-button {
            background-color: #555;
            color: white;
            padding: 16px 20px;
            border: none;
            cursor: pointer;
            opacity: 0.8;
            position: fixed;
            bottom: 23px;
            right: 28px;
            width: 280px;
        }

        /* The popup form - hidden by default */
        .form-popup {
            display: none;
            position: fixed;
            top: 0;
            right: 15px;
            border: 3px solid #f1f1f1;
            z-index: 9;
        }

        /* Add styles to the form container */
        .form-container {
            max-width: 300px;
            padding: 10px;
            background-color: white;
        }

        /* Full-width input fields */
        .form-container input[type=text], .form-container input[type=password] {
            width: 100%;
            padding: 15px;
            margin: 5px 0 22px 0;
            border: none;
            background: #f1f1f1;
            font-size: 1.5rem;
            p.capitalize {text-transform: capitalize;
            }
        }

        /* When the inputs get focus, do something */
        .form-container input[type=text]:focus, .form-container input[type=password]:focus {
            background-color: #ddd;
            outline: none;
        }

        /* Set a style for the submit/login button */
        .form-container .btn {
            background-color: #32CD32;
            color: white;
            padding: 16px 20px;
            border: none;
            cursor: pointer;
            width: 100%;
            margin-bottom:10px;
            opacity: 0.8;
            font-size: 2rem;
        }

        /* Add a red background color to the cancel button */
        .form-container .cancel {
            background-color: #FF4500;
        }

        /* Add some hover effects to buttons */
        .form-container .btn:hover, .open-button:hover {
            opacity: 1;
        }

        .homePage-w3-content{
            font-size: 1.2rem;
            background-color: #f2f2f2;
            margin-top: 70px;
            margin-bottom: 100px;
            margin-right: 0px;
            border: 5px solid #B22222;
            border-radius: 10px;
        }

        .homePage-w3-content input[type=text], .homePage-w3-content[type=date]{
            font-family: Arial;
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        .w3-top{
            display: flex;
        }
        .sr-only {
            position: absolute;
            width: 1px;
            height: 1px;
            padding: 0;
            margin: -1px;
            overflow: hidden;
            clip: rect(0,0,0,0);
            border: 0;
        }

    </style>
    </head>

    
    <body>

        <!-- Navbar (sit on top) -->
        <div class="w3-top w3-hide-small">
            <div class="w3-bar w3-xlarge w3-black w3-hover-opacity-off" id="myNavbar">
                <a href="homePageServlet" class="nav-link w3-bar-item w3-button w3-padding-large">Főoldal<span class="sr-only"></span></a>
                <a href="lostPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Elveszett</a>
                <a href="foundPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Megtalált</a>
                <a href="storyPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Sikertörténetek</a>
                <a href="aboutUsPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Rólunk</a>
                <c:if test="${pageContext.request.isUserInRole('user') || pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarUserPage.jsp" />
                    </div>
                </c:if>

                <c:if test="${pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarAdminPage.jsp" />
                    </div>
                </c:if>

                <div id="topmenu_navbar">
                    <jsp:include page="navBarLoginRegistrationPage.jsp" />
                </div>
            </div>
        </div>

        <div class="hero-image">
            <div class="w3-container" id="rapid">
                <div class="w3-content homePage-w3-content border border-danger rounded" style="max-width:600px;">
                    <h1 class="w3-center w3-padding-40"><span class="w3-tag w3-wide" style="background-color: #f2f2f2; color:red">BEJELENTÉS</span></h1>
                    <div class="w3-row w3-center w3-card w3-padding">
                        <a href="javascript:void(0)" onclick="openRapid(event, 'Lost');" id="myLink">
                            <div class="w3-col s6 tablink">ELVESZETT</div>
                        </a>
                        <a href="javascript:void(0)" onclick="openRapid(event, 'Found');">
                            <div class="w3-col s6 tablink">MEGTALÁLT</div>
                        </a>
                    </div>
                    <div id="Lost" class="w3-container rapid w3-padding-20 w3-card">
                        <form  name="myLostForm" action="lostFormServlet" method="post" onsubmit="return(validateLostForm());">
                            <button type="button" class="btn" data-toggle="modal" data-target="#myModal" data-remote="toUploadServlet">Fénykép feltöltése</button>
                            <a href="toUploadServlet" id="myUploadLink">Fénykép feltöltése</a><br>                           
                            <label for="image">
                                <input type="hidden" id="image" name="image" class="textfield" value="<%= request.getAttribute("imageLink")%>">
                            </label>
                            <label for="nickName">Kisállat neve:
                                <input id="nickName" name="nickName" class="textfield" type="text">
                            </label>
                            <label for="lastKnownPlace">Utolsó tartózkodási helye:
                                <input id="lastKnownPlace" name="lastKnownPlace" class="textfield" type="text">
                            </label>
                            <label for="lostDate">Mikor látták utoljára?
                                <input id="lostDate" name="lostDate" class="textfield" type="date">
                            </label>
                            <div class="form-group">
                                Állat neme:<br>
                                <label class="container">kan
                                    <input type="radio" checked="checked" value="male" name="sex">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container">szuka
                                    <input type="radio"  value="female" name="sex">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <label for="petProperties">Egyéb tudnivalók:
                                <input id="petProperties" name="petProperties" class="textfield" type="text">
                            </label>
                            <label for="lostName">Tulajdonos neve:
                                <input id="lostName" name="lostName" class="textfield" type="text">
                            </label>
                            <label for="lostPhone">Tulajdonos telefonszáma:
                                <input id="lostPhone" name="lostPhone" class="textfield" type="text">
                            </label>
                            <label for="lostMail">Tulajdonos e-mail címe:
                                <input id="lostMail" name="lostMail" class="textfield" type="text">
                            </label>
                            <div class="form-group">
                                <button type="submit">Bejelentés</button>
                            </div>
                        </form>
                    </div>

                    <div id="Found" class="w3-container rapid w3-padding-20 w3-card">
                        <form  name="myFoundForm" action="foundFormServlet" method="post" onsubmit="return(validateFoundForm());">
                            <a href="toUploadServlet" id="myUploadLink">Fénykép feltöltése</a><br>
                            <label for="image">
                                <input type="hidden" id="image" name="image" class="textfield" type="text" value="<%= request.getAttribute("imageLink")%>">
                            </label>
                            <label for="foundPlace">Hol találtam:
                                <input id="foundPlace" name="foundPlace" class="textfield" type="text">
                            </label>
                            Állat neme:<br>
                            <label class="container">kan
                                <input type="radio" checked="checked" value="male" name="sex">
                                <span class="checkmark"></span>
                            </label>
                            <label class="container">szuka
                                <input type="radio"  value="female" name="sex">
                                <span class="checkmark"></span>
                            </label>
                            <label for="petProperties">Egyéb tudnivalók:
                                <input id="petProperties" name="petProperties" class="textfield" type="text">
                            </label>
                            <label for="foundDate">Mikor találtam meg?
                                <input id="foundDate" name="foundDate" class="textfield" type="date">
                            </label>
                            <label for="foundName">Megtaláló neve:
                                <input id="foundName" name="foundName" class="textfield" type="text">
                            </label>
                            <label for="foundPhone">Megtaláló telefonszáma:
                                <input id="foundPhone" name="foundPhone" class="textfield" type="text">
                            </label>
                            <label for="foundMail">Megtaláló e-mail címe:
                                <input id="foundMail" name="foundMail" class="textfield" type="text">
                            </label>
                            <div class="form-group">
                                <button type="submit">Bejelentés</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <script>

            function openRapid(evt, rapidName) {
                var i, x, tablinks;
                x = document.getElementsByClassName("rapid");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                tablinks = document.getElementsByClassName("tablink");
                for (i = 0; i < x.length; i++) {
                    tablinks[i].className = tablinks[i].className.replace(" w3-dark-grey", "");
                }
                document.getElementById(rapidName).style.display = "block";
                evt.currentTarget.firstElementChild.className += " w3-dark-grey";
            }
            document.getElementById("myLink").click();


            $(function () {

                $("#myUploadDialog").dialog({
                    autoOpen: false,
                    modal: true
                });

                $("#myUploadLink").on("click", function (e) {
                    e.preventDefault();
                    $("#myUploadDialog").dialog("open");
                });

            });


            $("#upload").css('opacity', '0');

            $("#select_file").click(function (e) {
                e.preventDefault();
                $("#upload").trigger('click');
            });


            function validateLostForm() {

                if (document.myLostForm.lostDate.value === "") {
                    alert("Kérem adja meg az eltünés dátumát!");
                    document.myLostForm.lostDate.focus();
                    return false;
                }

                if (document.myLostForm.lostName.value === "") {
                    alert("Kérem adja meg a nevét!");
                    document.myLostForm.lostName.focus();
                    return false;
                }
                if (document.myLostForm.lostMail.value === "") {
                    alert("Kérem adja meg az email címét!");
                    document.myLostForm.lostMail.focus();
                    return false;
                }
				
				if (document.myLostForm.lostMail.value !== "") {
					var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
					if (!(document.myLostForm.lostMail.value.match(mailformat))){
						alert("Kérem érvényes email címet adjon meg!");
						document.myLostForm.lostMail.focus();
						return (false)
					}
				}

                return(true);
            }

            function validateFoundForm() {

                if (document.myFoundForm.foundDate.value === "") {
                    alert("Kérem adja meg a megtalálás dátumát!");
                    document.myFoundForm.foundDate.focus();
                    return false;
                }

                if (document.myFoundForm.foundName.value === "") {
                    alert("Kérem adja meg a nevét!");
                    document.myFoundForm.foundName.focus();
                    return false;
                }
                if (document.myFoundForm.foundMail.value === "") {
                    alert("Kérem adja meg az email címét!");
                    document.myFoundForm.foundMail.focus();
                    return false;
                }
				
				if (document.myFoundForm.foundMail.value !== "") {
					var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
					if (!(document.myFoundForm.foundMail.value.match(mailformat))){
						alert("Kérem érvényes email címet adjon meg!");
						document.myFoundForm.foundMail.focus();
						return (false);
					}
				}
				
                return(true);
            }
            
            function alertUpload() {
                alert("Sikeres Bejelentés!");
            }

        </script>

        <!-- Footer -->
        <footer class="w3-container w3-padding-64 w3-buttom w3-opacity w3-light-grey w3-xlarge">
            <div class="footer-copyright text-center py-3">© 2018 Copyright:
                <a href="https://wwf.hu/" style="color:#009900"> PetSaver</a>
            </div>
        </footer>
        <div id="myModal" class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> × </button>
                <h3 id="myModalLabel">Modal header</h3>
            </div>
            <div class="modal-body">        
                <div id="myUploadDialog" title="FileUpload">
                    <form action="fileUploadHandlerServlet" enctype="multipart/form-data" method="post" onsubmit="alertUpload()">
                        <input type="file" name="file2" /><br>
                        <input type="submit" value="upload" />
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </div>

    </body>
</html>

