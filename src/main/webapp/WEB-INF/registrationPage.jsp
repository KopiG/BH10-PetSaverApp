<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import = "java.io.*,java.util.*" %>
<!DOCTYPE html>
<html>
    <head>
        <title>RegistrationPage</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Amatic+SC">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
       <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/registration.css"/>
        <style>
            .registration-w3-top{
                font-family: "Amatic SC", sans-serif;
            }

            .regisztracio{
                background-image: url("resources/media/funnyCats.jpg");
                font-family: Arial;
                min-height: 100vh;
                background-size: cover;
                padding-top: 8%;
                padding-button: 8%;
            }

            .registration-card{
                background-color: rgba(42, 47, 51, .8);
                color: #fff;
            }

            .registration-card a{
                color: #eff0f7;
            }

            .registration-card-header{
                background-color: rgba(42, 47, 51, .8);
                letter-spacing: 3px;
                font-size: 1.2rem;
                font-weight: 600;
                color: #eff0f7;
            }

            .registration-card .form-control{
                background-color:rgba(42, 47, 51, .7);
                border-color: transparent;
            }

            .registration-card .form-control:focus{
                background-color:rgba(42, 47, 51, .5);
                border-color: #3384AA;
                outline: none;
                box-shadow: none;
                color: #eff0f7;
            }

            .registration-card .form-control::placeholder{
                color: #d6d7de;
                opacity: .8;
            }

            .btn-registration{
                background-color: #298e62;
                letter-spacing: 3px;
                font-weight: 600;
            }

            .btn-registration:hover{
                background-color: #2d9b6b;
            }
        </style>
    </head>
    
    <body class="regisztracio">
          
        <!-- Navbar (sit on top) -->
        <div class="w3-top registration-w3-top w3-hide-small">
            <div class="w3-bar w3-xlarge w3-black w3-hover-opacity-off" id="myNavbar">
                <a href="homePageServlet" class="nav-link w3-bar-item w3-button w3-padding-large">Főoldal<span class="sr-only"></span></a>
                <a href="lostPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Elveszett</a>
                <a href="foundPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Megtalált</a>
                <a href="storyPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Sikertörténetek</a>
                <a href="aboutUsPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Rólunk</a>
                <c:if test="${pageContext.request.isUserInRole('user') || pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarUserPage.jsp" />
                    </div>
                </c:if>
                
                <c:if test="${pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarAdminPage.jsp" />
                    </div>
                </c:if>
                
                <div id="topmenu_navbar">
                    <jsp:include page="navBarLoginRegistrationPage.jsp" />
                </div>
            </div>
        </div>
        
        <div class="container">    
            <div class="row">
                <div class="col-12 col-lg-6 mx-auto">
                    <div class="card registration-card">
                        <div class="card-header registration-card-header text-uppercase">
                            Regisztráció
                        </div>
                        <form action="registrationServlet" method="post">
                            <div class="card-body">
                                <section class="color-1">
                                    <div class="form-group">
                                        <label for="nickName">Add meg a beceneved</label>
                                        <input type="text" class="form-control" id="nickName" name="nickName" aria-describedby="textHelp" placeholder="becenév">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Add meg az e-mail címed</label>
                                        <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="valami@email.hu">
                                    </div>
                                    <div class="form-group">
                                        <label for="lastName">Add meg a vezetékneved</label>
                                        <input type="text" class="form-control" id="lastName" name="lastName" aria-describedby="textHelp" placeholder="vezetéknév">
                                    </div>
                                    <div class="form-group">
                                        <label for="firstName">Add meg a keresztneved</label>
                                        <input type="text" class="form-control" id="firstName" name="firstName" aria-describedby="textHelp" placeholder="keresztnév">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Add meg a jelszavad</label>
                                        <input type="password" class="form-control" id="password" name="password" placeholder="jelszó">
                                    </div>
                                    <div class="form-group">
                                        <label for="confirmPassword">Add meg a jelszavad újra</label>
                                        <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="jelszó újra">
                                    </div>
                                    <div class="card-footer">
<!--                                        <button type="button" class="btn btn-registration btn-secondary btn-lg btn-block text-uppercase">Regisztráció</button>-->
                                        <input type="submit" name="submit" value="Regisztráció" type="button" class="btn btn-registration btn-secondary btn-lg btn-block text-uppercase">
                                        <div class="pt-3"><small>Már regisztráltál?<a href="homePageServlet">Főoldal</a></small></div>
                                    </div>
                                </section>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>