<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import = "java.io.*,java.util.*" %>
<!DOCTYPE html>
<html>
    <head>
        <title>UserPage</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Amatic+SC">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <style>
            body, html {height: 100%}

            .userPage{
                background-color: #F2D5BB;
            }

            .top-w3-bar{
                font-family: "Amatic SC", sans-serif;
            }

            .footer-w3-container{
                margin-bottom: 0;
            }

            .user-container-fluid{
                font-family: "Arial", sans-serif;
                font-size: 1rem;
                color: black;
                margin-top: 20px;
            }

            .table-bejelento-lost {
                background-color: #fff;
                padding: 10px;
                box-shadow: 3px 3px 0 rgba(0,0,0,0.1);
                max-width: calc(100% - 2em);
                margin: 1em auto;
                overflow: hidden;
                width: 800px;
                border: none;
                box-shadow: none;
                overflow: visible;
            }

            table {
                width: 100%;

                td, th {
                    color: rgba(217, 136, 89, 1);

                }

                td {
                    text-align: center;
                    vertical-align: middle;

                    &:last-child {
                        font-size: 0.95em;
                        line-height: 1.4;
                        text-align: left;
                    }
                }

                th {
                    background-color: lighten;
                    font-weight: 300;
                }

                tr {
                    &:nth-child(2n) { background-color: white; }
                    &:nth-child(2n+1) { background-color: lighten }
                }
            }

            @media screen and (max-width: 700px) {
                table, tr, td { display: block; }

                td {
                    &:first-child {
                        position: absolute;
                        top: 50%;
                        transform: translateY(-50%);

                    }

                    &:not(:first-child) {
                        clear: both;
                        padding: 4px 20px 4px 90px;
                        position: relative;
                        text-align: left;

                        &:before {
                            color: lighten;

                            display: block;
                            left: 0;
                            position: absolute;
                        }
                    }

                    &:nth-child(2):before { content: 'Kép:'; }
                    &:nth-child(3):before { content: 'Neve:'; }
                    &:nth-child(4):before { content: 'Neme:'; }
                    &:nth-child(4):before { content: 'Város:'; }
                    &:nth-child(5):before { content: 'Elveszett:'; }
                }

                tr {
                    position: relative;
                    &:first-child { display: none; }
                }
            }

            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                background-color: red;
                color: white;
                text-align: center;
            }


        </style>
    </head>

    <body class="userPage">
        ​
        <!-- Navbar (sit on top) -->
        <div class="w3-top w3-hide-small">
            <div class="w3-bar top-w3-bar w3-xlarge w3-black w3-hover-opacity-off" id="myNavbar">
                <a href="homePageServlet" class="nav-link w3-bar-item w3-button w3-padding-large">Főoldal<span class="sr-only"></span></a>
                <a href="lostPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Elveszett</a>
                <a href="foundPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Megtalált</a>
                <a href="storyPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Sikertörténetek</a>
                <a href="aboutUsPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Rólunk</a>
                <c:if test="${pageContext.request.isUserInRole('user') || pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarUserPage.jsp" />
                    </div>
                </c:if>

                <c:if test="${pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarAdminPage.jsp" />
                    </div>
                </c:if>

                <div id="topmenu_navbar">
                    <jsp:include page="navBarLoginRegistrationPage.jsp" />
                </div>
            </div>
        </div>
        ​
        <div class="container-fluid user-container-fluid text-center">
            <div class="row content">
                ​<c:if test="${pageContext.request.isUserInRole('admin') || pageContext.request.isUserInRole('user')}">
                    <h1>Hello Önkéntesek!</h1>
                    ​
                    <h2>Talált kisállatok listája:</h2>
                    <div class="container p-3 mb-2 bg-white text-dark">
                        <c:if test="${not empty foundPets}">

                            <table id="table-bejelento-lost" class="table table-hover btn-table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Megtalált helyszín</th>
                                        <th>Neme</th>
                                        <th>Kisállat tulajdonságai</th>
                                        <th>Megtalálás dátuma</th>
                                        <th>Kép</th>
                                        <th>Megtaláló</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <br/>
                                        <c:forEach items="${foundPets}" var="item">
                                            <c:url var="updateLink" value="/updatePetServlet">
                                                <c:param name="id" value="${item.id}"/>
                                                <c:param name="foundPlace" value="${item.foundPlace}"/>
                                                <c:param name="sex" value="${item.sex}"/>
                                                <c:param name="petProperties" value="${item.petProperties}"/>
                                                <c:param name="foundDate" value="${item.foundDate}"/>
                                                <c:param name="image" value="${item.image}"/>
                                                <c:param name="announcer" value="${item.announcer.getEmailOfAnnouncer()}"/>
                                            </c:url>
                                            <td>${item.foundPlace}</td>
                                            <td>${item.sex}</td>
                                            <td>${item.petProperties}</td>
                                            <td>${item.foundDate}</td>
                                            <td><image src="DisplayImageServlet?name=${item.image}" data-toggle="modal" data-target="#myModal" style="height:100px; width:100px;"></td>       
                                            <td>${item.announcer.getEmailOfAnnouncer()}</td>
                                            <td><a href="${updateLink}">Szerkesztés</a></td>
                                        </c:forEach>
                                    </tr>                                  
                                </tbody>                               
                            </table>
                        </c:if>
                        <c:if test="${empty foundPets}">
                            <h4>Jelenleg nincs több talált kisállat.</h4>
                        </c:if>
                        ​
                    </div>

                    ​
                    <br/>
                    <br/>
                    ​
                    <h2>Eltűnt kisállatok listája:</h2>
                    <div class="container p-3 mb-2 bg-white text-dark">
                        <c:if test="${not empty lostPets}">
                            <table id="table-bejelento-found" class="table table-hover btn-table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Becenév</th>
                                        <th>Utoljára látták</th>
                                        <th>Neme</th>
                                        <th>Kisállat tulajdonságai</th>
                                        <th>Eltűnés dátuma</th>
                                        <th>Kép</th>
                                        <th>Elvesztő</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <c:forEach items="${lostPets}" var="item">
                                            <c:url var="updateLink" value="/updatePetServlet">
                                                <c:param name="id" value="${item.id}"/>
                                                <c:param name="nickName" value="${item.nickName}"/>
                                                <c:param name="lastKnownPlace" value="${item.lastKnownPlace}"/>
                                                <c:param name="sex" value="${item.sex}"/>
                                                <c:param name="petProperties" value="${item.petProperties}"/>
                                                <c:param name="lostDate" value="${item.lostDate}"/>
                                                <c:param name="image" value="${item.image}"/>
                                                <c:param name="announcer" value="${item.announcer.getEmailOfAnnouncer()}"/>
                                            </c:url>
                                            <td>${item.nickName}</td>
                                            <td>${item.lastKnownPlace}</td>
                                            <td>${item.sex}</td>
                                            <td>${item.petProperties}</td>
                                            <td>${item.lostDate}</td>
                                            <td><image src="DisplayImageServlet?name=${item.image}" data-toggle="modal" data-target="#myModal" style="height:100px; width:100px;"></td>       
                                            <td>${item.announcer.getEmailOfAnnouncer()}</td>
                                            <td><a href="${updateLink}">Szerkesztés</a></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>                               
                            </table>
                        </c:if>
                        <c:if test="${empty lostPets}">
                            <h4>Jelenleg nincs több eltűnt kisállat.</h4>
                        </c:if>
                    </div>
                </c:if>
            </div>
        </div>


        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <img class="showimage img-responsive" src="" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        ​
        <!-- Footer -->
        <footer class="w3-container w3-padding-64 w3-buttom w3-opacity w3-light-grey w3-xlarge">
            <div class="footer-copyright text-center py-3">© 2018 Copyright:
                <a href="https://wwf.hu/" style="color:#009900"> PetSaver</a>
            </div>
        </footer>
        <script>
            $(document).ready(function () {
                $('img').on('click', function () {
                    var image = $(this).attr('src');
                    //alert(image);
                    $('#myModal').on('show.bs.modal', function () {
                        $(".showimage").attr("src", image);
                    });
                });
            });
        </script>
    </body>
</html>