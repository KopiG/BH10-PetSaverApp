<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import = "java.io.*,java.util.*" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Story Page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Amatic+SC">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    </head>
    <style>
        body, html {height: 100%}
        body,h1,h2,h3,h4,h5,h6 {font-family: "Amatic SC", sans-serif}

        .storyPage{
            background-color: #F5F5DC;
        }

        b{
            font-family: "Arial", sans-serif;
            font-size: 1.6rem;
        }
        .content{
            font-family: "Arial", sans-serif;
            font-size: 1.2rem;

        }

        p {
            margin-left: 8%;
            margin-right: 8%;
        }


    </style>
    <body class="storyPage">

        <div class="w3-top w3-hide-small">
            <div class="w3-bar w3-xlarge w3-black w3-hover-opacity-off" id="myNavbar">
                <a href="homePageServlet" class="nav-link w3-bar-item w3-button w3-padding-large">Főoldal<span class="sr-only"></span></a>
                <a href="lostPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Elveszett</a>
                <a href="foundPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Megtalált</a>
                <a href="storyPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Sikertörténetek</a>
                <a href="aboutUsPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Rólunk</a>
                <c:if test="${pageContext.request.isUserInRole('user') || pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarUserPage.jsp" />
                    </div>
                </c:if>

                <c:if test="${pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarAdminPage.jsp" />
                    </div>
                </c:if>

                <div id="topmenu_navbar">
                    <jsp:include page="navBarLoginRegistrationPage.jsp" />
                </div>
            </div>
        </div>

        <div class="container-fluid text-center">    
            <div class="row content">
                <div style="background-color: rgba(0,0,0,0.4)"> 
                    <h1 style="font-size:2vw; color:white">Történetek:</h1>
                    <br/>
                    <br/>
                    <h1><b>Mangó sikertörténete</b></h1>
                    <p>Korábbi védencünk, Mango 2 hónapja él szerető családban, gazdája levelet és képeket küldött nekünk:</p>
                    <p>"Kiskutyám, Mango lassan 2 hónapja boldogítja a napjainkat! Nagyon belopta magát mindenki szívébe. Mostanra sokmindent megtanult, suliba is járunk. Remélhetőleg hamarosan elkezdjük a terápias kiképzést is. Nagyon boldogok vagyunk, bejártuk már a fél országot és nem állunk meg a határoknál sem!"</p>
                    <p>Nagyon örülünk, hogy Mango élete ilyen jóra fordult, hosszú, boldog közös éveket és további izgalmas kirándulásokat, felejthetetlen kalandokat kívánunk neki és gazdáinak!</p>
                    <img src="resources/media/mango.jpg">

                    <br/>
                    <br/>
                    <br/>
                    <h1><b>Zengő sikertörténete</b></h1>
                    <p>Zengő már 4 éve a kis barátom, leghűségesebb kis társam, egy szóval a kutyám. Most érkeztünk el oda, hogy valóban felnőtt kutya lett, beért a kis buksija és hihetetlenül jól tudunk együtt dolgozni. Szerencsére ennek ellenére továbbra is egy bolondgomba. Ügyesedünk agilityben, kipróbáltuk a terelést és rengeteg helyen voltunk idén is együtt kirándulni. Igazi barátai is lettek. Az a félős kiskutya, akit befogadtam, rengeteget alakult, ma már szinte nincs olyan hely, ahova ne tudnám magammal vinni, minden akadályt és új helyzetet ügyesen vesz. Kívánom, hogy sokan találjanak nálatok ilyen kis kincseket </p>
                    <p>Boldog Karácsonyt és szép ünnepeket kívánunk nektek, gondos, szerető gazdikat a kis védenceiteknek.</p>
                    <img src="resources/media/zengo.jpg">

                    <br/>
                    <br/>
                    <br/>
                    <h1><b>Zsozsó sikertörténete</b></h1>
                    <p>Szeretnék beszámolni a mi kis Zsozsó barátunk eddigi itteni életéről. Az első héten, ahogy idekerült, nem nagyon evett, a kedve viszont jó volt, élénk volt, ezért azt gondoltam talán válogatós.</p>
                    <p>Most már nagyon jól eszik, étrendje főzött csirke aprólék, táp, kenyér vagy főtt rizs. Unaloműzőnek, a magányos órákra pedig marhacsontot kap.

                        Nagyon gyorsan beilleszkedett a társasházi életbe. Ha egy kis időre nem vagyunk itthon, szobakennelben van, a bezártsághoz a menhelyen hozzá szokott, úgyhogy ezzel sem volt probléma. Kétszer ugatott eleinte, de ezt is le tudtuk vele tisztázni, azóta nem hallottam hangját.
                    </p>
                    <img src="resources/media/zsozso.jpg">
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                </div>

            </div>
        </div>

        <footer class="w3-container w3-padding-64 w3-buttom w3-opacity w3-light-grey w3-xlarge">
            <div class="footer-copyright text-center py-3">© 2018 Copyright:
                <a href="https://wwf.hu/" style="color:#009900"> PetSaver</a>
            </div>
        </footer>
    </body>
</html>
