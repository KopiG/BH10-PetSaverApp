<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import = "java.io.*,java.util.*" %>
<!DOCTYPE html>
<html>
    <head>
        <title>FoundPage</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Amatic+SC">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <style>
        body, html {height: 100%}
        body,h1,h2,h3,h4,h5,h6 {font-family: "Amatic SC", sans-serif}
     </style>
    </head>
    
    <body>
          
        <!-- Navbar (sit on top) -->
        <div class="w3-top w3-hide-small">
            <div class="w3-bar w3-xlarge w3-black w3-opacity w3-hover-opacity-off" id="myNavbar">
                <a href="homePageServlet" class="nav-link w3-bar-item w3-button w3-padding-large">Főoldal<span class="sr-only"></span></a>
                <a href="lostPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Elveszett</a>
                <a href="foundPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Megtalált</a>
                <a href="storyPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Sikertörténetek</a>
                <a href="aboutUsPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Rólunk</a>
                <c:if test="${pageContext.request.isUserInRole('user') || pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarUserPage.jsp" />
                    </div>
                </c:if>
                
                <c:if test="${pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarAdminPage.jsp" />
                    </div>
                </c:if>
                
                <div id="topmenu_navbar">
                    <jsp:include page="navBarLoginRegistrationPage.jsp" />
                </div>
            </div>
        </div>
        
        <div class="container-fluid text-center">    
            <div class="row content">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <div style="background-color: rgba(0,0,0,0.4)" class="col-sm-8 text-left"> 
                <h1 style="font-size:2vw; color:white">Megtalált kisállatok:</h1>
                    <div class="container p-3 mb-2 bg-white text-dark">
                        <c:if test="${not empty foundPetsList}">
                             <form action="foundPetEditServlet" method="get" id="foundPetEditServlet">
                            <table class="table">
                                <th>Macska vagy Kutya?</th>
                                <th>Tipus</th>
                                <th>Neme</th>
                                <th>Kora</th>
                                <th>Szine</th>
                                <th>Súlya</th>
                                <th>Kép</th>
                                <th></th>
                                <c:forEach items="${foundPetsList}" var="item">
                                    <c:url var="updateLink" value="foundPetEditServlet">
                                        <c:param name="petId" value="${item.id}"/>
                                        <c:param name="catOrDog" value="${item.catOrDog}"/>
                                        <c:param name="type" value="${item.type}"/>
                                        <c:param name="sex" value="${item.sex}"/>
                                        <c:param name="age" value="${item.age}"/>
                                        <c:param name="color" value="${item.color}"/>
                                        <c:param name="weight" value="${item.weight}"/>
                                        <c:param name="image" value="${item.image}"/>
                                    </c:url>
                                <tr>
                                    <c:if test="${item.catOrDog == 'TRUE'}">
                                        <td>Kutya</td>
                                    </c:if>
                                    <c:if test="${item.catOrDog == 'FALSE'}">
                                        <td>Macska</td>
                                    </c:if>
                                    <td>${item.type}</td>
                                    <c:if test="${item.sex == 'TRUE'}">
                                        <td>Him</td>
                                    </c:if>
                                    <c:if test="${item.sex == 'FALSE'}">
                                        <td>Nőstény</td>
                                    </c:if>
                                    <td>${item.age}</td>
                                    <td>${item.color}</td>
                                    <td>${item.weight}</td>
                                    <td><image src="DisplayImageServlet?name=${item.image}" height="100" width="100"></td>
                                    <td><a href="foundPetAdditionalInfoServlet">Bövebb info</a></td>
                                    <td><input type="hidden" id="petId" name="petId" value="${item.id}"></td>                       
                                    <td><button type="submit" form="foundPetEditServlet" value="submit">Szerkesztés</button></td>                                    
                                </tr>
                                </c:forEach>
                            </table>
                             </form>
                        </c:if>
                        <c:if test="${empty foundPetsList}">
                            <h3>Jelenleg nincs talált kisállat</h3>
                        </c:if>              ​
                    </div>
                </div>   
            </div>
        </div>

         <!-- Footer -->
        <footer class="w3-container w3-padding-64 w3-center w3-opacity w3-light-grey w3-xlarge">
            <div class="footer-copyright text-center py-3">© 2018 Copyright:
                <a href="https://wwf.hu/" style="color:#009900"> PetSaver</a>
            </div>
        </footer>
    </body>
</html>
