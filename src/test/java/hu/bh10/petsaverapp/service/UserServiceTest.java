package hu.bh10.petsaverapp.service;

import hu.bh10.petsaverapp.dao.UserDAO;
import hu.bh10.petsaverapp.dto.UserDTO;
import hu.bh10.petsaverapp.entity.UserEntity;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.mockito.Mock;

@RunWith(org.mockito.junit.MockitoJUnitRunner.class)
public class UserServiceTest {
    
    private UserService us;
    
    @Mock
    UserDAO userDAO;
    
    public UserServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        us = new UserService(userDAO);
    }
    
    @After
    public void tearDown() {
    }

    @org.junit.Test
    public void testRegisterUser_validUserWithValidPassword_passwordIsHashed() throws Exception {
        UserDTO user = new UserDTO();
        user.setUserPassword("steve");

        UserEntity ue =  us.registerUser(user);
        Assert.assertNotEquals(user.getUserPassword(), ue.getUserPassword());
        
    }
    
}
